"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_substances tests *

:details: lara_django_substances application urls tests.
         - 
:authors: mark doerr  <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

from django.test import TestCase
from django.urls import resolve, reverse

# from lara_django_substances.models import

# Create your lara_django_substances tests here.
