#! /usr/bin/env python3
"""_____________________________________________________________________

:PROJECT: LARA

* ementing substance csv filse *

:details: this corrects errors in substance csv files before 
          importing them into LARA substance database.

          Usage:  ./ement_subst_csv.py -i subst_orig.csv -o subst_em.csv

:authors: mark doerr mark.doerr@uni-greifswald.de

:date: (creation)         20200418

.. note:: -
.. todo:: -
________________________________________________________________________
"""
__version__ = "0.0.1"

import os
import sys
import re
import json
import csv
import logging
import argparse
from collections import defaultdict

def correct_subtance_csv(subst_csv_filename, subst_corr_csv_filename):
    # for statistics
    vendors = defaultdict(int)
    locations = defaultdict(int)
    rows_num = 0
    
    try:
        with open(subst_csv_filename, 'r') as csv_infile:
            with open(subst_corr_csv_filename, 'w') as csv_outfile:
    
                csv_reader = csv.DictReader(csv_infile)
                fieldnames =  csv_reader.fieldnames + ['amount_mass', 'amount_vol']

                csv_out_writer = csv.DictWriter(csv_outfile, fieldnames=fieldnames, dialect='excel', quoting=csv.QUOTE_NONNUMERIC)
                csv_out_writer.writeheader()

                sys.stdout.write(f"now adding substances from csv file: [{subst_csv_filename}]...")
            
                for substance_row in csv_reader:
                    logging.debug(substance_row)
                    logging.debug(substance_row['substance_name'])
                    logging.debug(substance_row['vendor'])

                    if substance_row['substance_name'] == '' :
                        # skip empty rows
                        continue

                    # doing some statistics
                    rows_num += 1
                    vendors[substance_row['vendor']] += 1
                    locations[substance_row['location']] += 1

                    emend_substance_row(substance_row)
                    csv_out_writer.writerow(substance_row)

    except ValueError as err:
        sys.stderr.write(f'file {err}, ValueError: {subst_csv_filename}\n')
        exit()
    except KeyError as err:
        sys.stderr.write(f'file {subst_csv_filename}, KeyError: {err}\n Please check, if file is exported with quote delimited strings\n')
        exit()
    except Exception as err:
        sys.stderr.write(f'file {subst_csv_filename}, GeneralError: {err}\n')
        exit()

    vendors_sorted = sorted(vendors.items(), key=lambda kv: kv[1], reverse=True)
    locations_sorted = sorted(locations.items(), key=lambda kv: kv[1], reverse=True)
    logging.debug(f"\n---- vendors: \n{vendors_sorted}\n---- locations:\n{locations_sorted}\n")
    logging.debug(f"number of substance: {rows_num} ")

    sys.stdout.write("All substances added ! Have a nice day ;)\n")

def emend_substance_row(substance_row: list):
        """Emend and filter each substance row to remove human errors

        :param substance_row: row of a csv file
        :type substance_row: list
        """
        # convert amount to amount_volume or mass 
        logging.debug(substance_row['amount'])

        # creating default columns
        substance_row['amount_mass'] = 0.0
        substance_row['amount_vol'] = 0.0

        # finding mass or volume in amount column
        matches = re.search(r"(?P<amount>\d\.*\d*)\s*(?P<unit>[m,k]*[g,l])\s*", substance_row['amount'].lower())
        matches_fact = re.search(r"\s*(?P<factor>\d,*\d*)x(?P<amount>\d\.*\d*)\s*(?P<unit>[m,k]*[g,l])\s*", substance_row['amount'].lower())
        #matches = re.search(r"\s*((?P<factor>\d,*\d*)x)(?P<amount>\d,*\d*)\s*(?P<unit>[m,k]*[g,l])\s*", substance_row['amount'].lower())

        if matches is not None: mg = matches.group

        factor = 1.0 if matches_fact is None else float(matches_fact.group('factor'))

        try:
            amount = float(mg('amount')) * factor

            unit = mg('unit')
        except Exception as err:
            sys.stderr.write(f"ERROR({err})\n")
        

        try:
            if unit == 'kg':
                substance_row['amount_mass'] = amount 
            elif unit == 'g':
                substance_row['amount_mass'] = amount * 1E-3 
            elif unit == 'mg':
                substance_row['amount_mass'] = amount * 1E-3
            elif unit.lower() == 'l' :
                substance_row['amount_vol'] = amount
            elif unit.lower() == 'ml' :
                substance_row['amount_vol'] = amount * 1E-3
        except Exception as err:
            sys.stderr.write(f"ERROR({err})\n")

        # correcting vendor
        substance_row['vendor'] = substance_row['vendor'].replace(" ","").strip().lower()
        if substance_row['vendor'] == "suppelco" :  
            substance_row['vendor'] = "mm"
         

def parse_command_line():
    """
    parsing command line arguments
    """
    parser = argparse.ArgumentParser(description="Django Settings generator")
    parser.add_argument('-i', '--input_csv',  action='store', required=True,
                        default="substances.csv", help='substances csv file')

    parser.add_argument('-o', '--output_csv',  action='store',
                        default="substances_corrected.csv", help='corrected output file')

    parser.add_argument('-v', '--version', action='version', version='%(prog)s ' + __version__)

    return parser.parse_args()


if __name__ == '__main__':
    # or use logging.INFO (=20) or logging.ERROR (=30) for less output
    logging.basicConfig(format='%(levelname)-8s| %(module)s.%(funcName)s: %(message)s', level=logging.DEBUG)

    parsed_args = parse_command_line()

    correct_subtance_csv(parsed_args.input_csv, parsed_args.output_csv)