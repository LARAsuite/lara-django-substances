"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_substances models *

:details: lara_django_substances database models.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - remove unwanted models/fields
________________________________________________________________________
"""

import logging
import datetime
import uuid
from random import randint

from django.utils import timezone
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from django.db import models
from lara_django_sequences.models import Sequence

from lara_django_base.models import DataType, MediaType, ExtraDataAbstr, Tag, PhysicalStateMatter
from lara_django_people.models import LaraUser


settings.FIXTURES += []


class ExtraData(ExtraDataAbstr):
    """This class can be used to extend data, by extra information,
       e.g. more telephone numbers, customer numbers, ... """
    extra_data_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)  # models.AutoField(primary_key=True)
    file = models.FileField(
        upload_to='substances', blank=True, null=True, help_text="rel. path/filename")
    image = models.ImageField(upload_to='substances/images/', blank=True, default="image.svg",
                              help_text="extra substance image")


class SubstanceClass(models.Model):
    """ classes for substances, like amino acid, peptide, protein, nucleotide, DNA,... """

    # models.AutoField(primary_key=True)
    class_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    substance_class = models.TextField(
        unique=True, help_text="like amino acid, peptide, protein, nucleotide, DNA")
    description = models.TextField(
        blank=True, help_text="description of the substance class")

    def __str__(self):
        return self.substance_class or ""

    class Meta:
        verbose_name_plural = 'SubstanceClasses'


class PhysicalState(models.Model):
    """ physical states, like solid, liquid, gas, plasma,... """

    # models.AutoField(primary_key=True)
    state_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    state = models.TextField(
        unique=True, help_text="physical state, like solid, liquid, gas, plasma,...")
    description = models.TextField(
        blank=True, help_text="description of the substance state")

    def __str__(self):
        return self.state or ""


class SubstanceAbstr(models.Model):
    """ abstract base class for all substances """
    name = models.TextField(
        blank=True, help_text="most common name of the substance")
    acronym = models.TextField(
        blank=True, null=True, help_text="acronym, e.g. TFA, DMSO, MBA, 160323MBA")
    synonyms = models.TextField(
        blank=True, help_text="comma saparated list of synonymes, like 'benzene', 'amylalcohol' ")
    substance_class = models.ManyToManyField(SubstanceClass, related_name="%(app_label)s_%(class)s_extra_data_related",
                                             related_query_name="%(app_label)s_%(class)s_extra_data", blank=True,
                                             help_text="substance class, e.g. salt, complex, nucleotide")
    UUID = models.UUIDField(
        default=uuid.uuid4, help_text="LARA substance UUID")
    IRI = models.URLField(
        blank=True,  null=True, unique=True, max_length=512, help_text="International Resource Identifier - IRI: is used for semantic representation ")
    CAS = models.TextField(blank=True, help_text="Chemical Abstract ")
    IUPAC_name = models.TextField(blank=True, help_text="IUPAC name")
    PubChemID = models.TextField(blank=True, help_text="PubChem ID")
    PubChemFingerprint = models.TextField(
        blank=True, help_text=" s. ftp://ftp.ncbi.nlm.nih.gov/pubchem/specifications/pubchem_fingerprints.txt")
    ChemSpiderID = models.TextField(blank=True, help_text="s. chemspider.com")
    BeilsteinRegNum = models.TextField(
        blank=True, help_text="Beilstein Registry Number")
    EC_num = models.TextField(blank=True, help_text="EC number")
    MDL_num = models.TextField(blank=True, help_text="MDL number")
    PDB_code = models.TextField(blank=True, help_text="Protein Database Code")
    computed_properties = models.JSONField(
        blank=True, null=True, help_text="computed physical and chemical properties JSON")
    mwt = models.DecimalField(max_digits=32, decimal_places=16,
                              blank=True, null=True, help_text="molecular weight in g/mol")
    density = models.DecimalField(
        max_digits=20, decimal_places=9, blank=True, null=True, help_text="density in g /cm3")
    physical_state_matter = models.ForeignKey(PhysicalStateMatter, related_name="%(app_label)s_%(class)s_state_of_matter_related",
                                        related_query_name="%(app_label)s_%(class)s_state_of_matter_related_query",
                                        on_delete=models.CASCADE, null=True, blank=True,
                                        help_text="physical state of matter at 293 K")
    properties = models.JSONField(
        blank=True, null=True, help_text="physical and chemical properties JSON")
    sumformula = models.TextField(
        blank=True, help_text="sum formula of the substance")
    # note also the version of the files in metadata
    CML = models.TextField(
        blank=True, help_text="Chemical Markup Language, s. ")
    CDXML = models.TextField(blank=True, help_text="ChemDraw XML")
    SMILES = models.TextField(blank=True, help_text="SMILES")
    IsoSMILES = models.TextField(blank=True, help_text="isomeric SMILES")
    CanSMILES = models.TextField(blank=True, help_text="canonical SMILES")
    InChI = models.TextField(blank=True, help_text="InChi")
    InChIKey = models.TextField(blank=True, help_text="InChiKey")
    StdInChI = models.TextField(blank=True, help_text="StdInChi")
    StdInChIKey = models.TextField(blank=True, help_text="StdInChiKey")
    PDB = models.TextField(blank=True, help_text="pdb 3D structure file data")
    PDB_URL = models.URLField(
        blank=True, help_text="Universal Resource Locator - URL to external structure database, like Protein Data Base (PDB)")
    URL = models.URLField(
        blank=True, max_length=512, help_text="Universal Resource Locator - URL to external structure database,  link to public structure database")
    URL_definition = models.URLField(
        blank=True, help_text="definition Universal Resource Locator - URL, like wikipedia")
    LARA_structure_URL = models.URLField(
        blank=True, max_length=512, help_text="Universal Resource Locator - URL to external structure database, link to lara-django-structure database")
    mol_raw = models.TextField(blank=True, help_text="mol file raw")
    mol_2D = models.TextField(blank=True, help_text="mdl mol file 2D")
    mol_3D = models.TextField(blank=True, help_text="mdl mol file 3D")
    svg = models.TextField(blank=True, null=True,
                           help_text="svg image of the substance")
    image = models.ImageField(upload_to='substances/', blank=True, null=True,
                              help_text="e.g. png image of substance")  # rel. path/filename to image
    extra_data = models.ManyToManyField(ExtraData, related_name="%(app_label)s_%(class)s_extra_data_related",
                                        related_query_name="%(app_label)s_%(class)s_extra_data_related_query", blank=True,
                                        help_text="extra data, like absorption coefficient")
    literature = models.URLField(
        null=True, blank=True, help_text="literature and references of general type for the substance")
    # literature = models.ManyToManyField(LibItem, blank=True, related_name="%(app_label)s_%(class)s_literature_related",
    #                                     related_query_name="%(app_label)s_%(class)s_literature_related_query",
    #                                     help_text="literature and references of general type for the substance class")
    tags = models.ManyToManyField(Tag, blank=True, related_name="%(app_label)s_%(class)s_tags_related",
                                  related_query_name="%(app_label)s_%(class)s_tags_related_query",
                                  help_text="tags")
    remarks = models.TextField(
        blank=True, null=True, help_text="some remarks to this substance")
    description = models.TextField(
        blank=True, null=True, help_text="description of the substance")

    def __str__(self):
        return self.name or ""

    # def __repr__(self):
    #     return self.name or ""

    def __repr__(self):
        return f"{self.name} ({self.description})" or ""

    class Meta:
        abstract = True


class Isotope(SubstanceAbstr):
    """_Istope - detailed information about all known isotopes_

    :param SubstanceAbstr: _description_
    :type SubstanceAbstr: _type_
    :return: _description_
    :rtype: _type_
    :yield: _description_
    :rtype: _type_
    """
    isotope_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    num_protons = models.SmallIntegerField(
        blank=True, null=True, help_text="number of protons")
    num_protons = models.SmallIntegerField(
        blank=True, null=True, help_text="number of neutrons")
    # exact mass
    #
    electron_config = models.TextField(
        blank=True, help_text="electron configuration: e.g. 1s2, 2s2 3p3 ")
    # electronegativity
    # electron affinity
    # ion radius
    # van der Waals radius

    # halflifetime
    # rel. abundancies: earth, moon universe
    # detailed references (international institiute)


class Substance(SubstanceAbstr):
    """ substance can be an element or a compound (polymers, like DNA, peptides, proteins have their own class)
    it can mean a general compound or concrete synthesize product,
    analytical data should be addable (like UV-vis, NMR, HPLC, .... )
    """

    # models.AutoField(primary_key=True)
    substance_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    m1_code = models.TextField(
        blank=True, help_text="monomer one letter code, like 'A', 'T', 'C' ")
    m3_code = models.TextField(
        blank=True, help_text="monomer three letter code, like 'Gly' ")
    m5_code = models.TextField(
        blank=True, help_text="monomer five letter code")
    logP = models.DecimalField(max_digits=20, decimal_places=9,
                                blank=True, null=True, help_text="experimental log of octanol/water partition coefficient")
    AlogP = models.DecimalField(
        max_digits=20, decimal_places=9, blank=True, null=True, help_text="calculated log of octanol/water partition coefficient")
    XlogP = models.DecimalField(max_digits=20, decimal_places=9,
                                blank=True, null=True, help_text="calculated log of octanol/water partition coefficient")
    melting_point = models.DecimalField(
        max_digits=20, decimal_places=9, blank=True, null=True, help_text="in deg K")
    boiling_point = models.DecimalField(
        max_digits=20, decimal_places=9, blank=True, null=True, help_text="in deg K")


class Polymer(SubstanceAbstr):
    """__Defined polymers (no mixtures)__
    The polymer class defines how the sequence data is to be interpreted
    DNA_FASTA, DNA_FASTAQ, DNA_GeneBank,
    e.g. DNA1L, RNA1L, Protein1L, Protein3L 
    for sequences add expression host and original organism
    units are the same as in substance
    analytical data should be addable (like UV-vis, NMR, HPLC, .... )
    todo: taxanomic info could be added as separate model / app
          host_organism ? could be added to extra data

    !!! mind that many polymers (e.g, PE, PP, PET are mixtures)
    """

    # models.AutoField(primary_key=True)
    polymers_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    sequence = media_type = models.ForeignKey(Sequence, related_name="%(app_label)s_%(class)s_sequences_related",
                                              on_delete=models.CASCADE, null=True, blank=True,
                                              help_text="sequence of the polymer")
    melting_point = models.DecimalField(
        max_digits=20, decimal_places=18, null=True, blank=True, help_text="in deg K")
    boiling_point = models.DecimalField(
        max_digits=20, decimal_places=9, null=True, blank=True, help_text="in deg K")
    media_type = models.ForeignKey(MediaType, related_name="%(app_label)s_%(class)s_extra_data_related",
                                   on_delete=models.CASCADE, null=True, blank=True,  help_text="file type of sequence data (file)")
    sequence_file = models.FileField(
        upload_to='lara_polymer/', blank=True, null=True, help_text="rel. path/filename")


class MixtureComponent(models.Model):
    """Single component of a mixture"""
    mix_comp_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)  # models.AutoField(primary_key=True)
    name = models.TextField(blank=True, help_text="name of mixture component")
    IRI = models.URLField(
        blank=True,  null=True, unique=True, max_length=512, help_text="International Resource Identifier - IRI: is used for semantic representation ")
    substance = models.ForeignKey(Substance, related_name='%(app_label)s_%(class)s_substances_related',
                                  related_query_name="%(app_label)s_%(class)s_substances_related_query", blank=True, null=True,
                                  on_delete=models.CASCADE, help_text="ID of the substance")
    polymer = models.ForeignKey(Polymer, related_name='%(app_label)s_%(class)s_polymers_related', blank=True, null=True,
                                related_query_name="%(app_label)s_%(class)s_polymers_related_query",
                                on_delete=models.CASCADE, help_text="ID of the polymer")
    concentration = models.DecimalField(max_digits=20, decimal_places=9, blank=True, null=True,
                                        help_text="concentration in mol/l")  # this is used for mixtures
    concentration_weight = models.DecimalField(max_digits=20, decimal_places=9, blank=True, null=True,
                                               help_text="concentration in kg/l")  # this is used for mixtures
    description = models.TextField(
        blank=True, null=True, help_text="description")

    def __str__(self):
        return self.name or ""


class Mixture(SubstanceAbstr):
    """ a mixture is a mixture of substances or polymers, e.g. a buffer, a medium, an assay mixture
        analytical data should be addable (like UV-vis, NMR, HPLC, .... )
        acronyms: PBS, LB, ....
        will be removed with store ....
    """
    # models.AutoField(primary_key=True)
    mixture_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    components = models.ManyToManyField(MixtureComponent, related_name='%(app_label)s_%(class)s_components_related',
                                        related_query_name="%(app_label)s_%(class)s_components_related_query",
                                        help_text="components of the mixture")
    physical_state_matter = models.ForeignKey(PhysicalStateMatter, related_name="%(app_label)s_%(class)s_state_of_matter_related",
                                        related_query_name="%(app_label)s_%(class)s_state_of_matter_related_query",
                                        on_delete=models.CASCADE, null=True, blank=True,
                                        help_text="physical state of matter at 293 K")


class MoleculeResidues(models.Model):
    """_Residues of a molecule_

    :param models: _description_
    :type models: _type_
    :return: _description_
    :rtype: _type_
    :yield: _description_
    :rtype: _type_
    """
    residue_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(
        blank=True, help_text="name of the residue")
    substructure = models.TextField(
        blank=True, help_text="substructure of the residue")
    IRI = models.URLField(
        blank=True,  null=True, unique=True, max_length=512, help_text="International Resource Identifier - IRI: is used for semantic representation ")

    def __str__(self):
        return self.name or ""


class Reactand(models.Model):
    """Reactands are the components of a reaction. They are specific for a certain reaction, 
       since they combine the substance information with the stoichometric information
       NOTE: is backpointer to related reaction necessary ?   
    """

    # models.AutoField(primary_key=True)
    reactand_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    IRI = models.URLField(
        blank=True,  null=True, unique=True, max_length=512, help_text="International Resource Identifier - IRI: is used for semantic representation ")
    substance = models.ForeignKey(Substance, related_name='%(app_label)s_%(class)s_substances_related',
                                  related_query_name="%(app_label)s_%(class)s_substances_related_query", on_delete=models.CASCADE,
                                  help_text="ID of the substance")
    polymer = models.ForeignKey(Polymer, related_name='%(app_label)s_%(class)s_polymers_related',
                                related_query_name="%(app_label)s_%(class)s_polymers_related_query", on_delete=models.CASCADE,
                                help_text="ID of the polymer")
    mixture = models.ForeignKey(Mixture, related_name='%(app_label)s_%(class)s_mixtures_related',
                                related_query_name="%(app_label)s_%(class)s_mixtures_related_query", on_delete=models.CASCADE,
                                help_text="ID of the mixture")
    order = models.IntegerField(
        null=True, blank=True, help_text="order in the reaction, in case order of the reactands plays a role")
    stoichiometric_factor = models.DecimalField(max_digits=9, decimal_places=3, default=1.0, null=True, blank=True,
                                                help_text="stoichiometric_factor: neg: educt, pos: product")
    role = models.ManyToManyField(SubstanceClass, related_name='%(app_label)s_%(class)s_roles_related',
                                  related_query_name="%(app_label)s_%(class)s_roles_related_query", blank=True,
                                  help_text="roles of the reactand, like educt, product, solvent, catalyst, role educt, product, solvent, co-factor")
    physical_state = models.ForeignKey(PhysicalState, on_delete=models.CASCADE,
                                       help_text="e.g. solid, liquid, gas, plasma at reaction conditions")
    # TODO: add ManyToManyField for literature
    literature = models.URLField(
        null=True, blank=True, help_text="literature and references")
    # literature = models.ManyToManyField(
    #     LibItem, blank=True, help_text="literature and references")

    def __str__(self):
        return self.substance.name or self.polymer.name or self.mixture.name or ""


class Reaction(models.Model):
    """ abstractum of a chemical reaction
        real reactions are described in procedures (with devices, preparation steps, etc) 
        also reaction conditions will be mentioned there
       NOTE: location ??
       TODO: side reactions, speed, order of kinetics, ..., entalpy, entropy, ...

    """

    # models.AutoField(primary_key=True)
    reaction_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, help_text="reaction name")
    label = models.TextField(blank=True, help_text="(short) label / acronym")
    RX_UUID = models.UUIDField(blank=True, null=True, unique=True, default=uuid.uuid4,
                               help_text="reaction UUID")
    IRI = models.URLField(
        blank=True,  null=True, unique=True, max_length=512, help_text="International Resource Identifier - IRI: is used for semantic representation ")
    components = models.ManyToManyField(
        Reactand, related_name='%(app_label)s_%(class)s_components_related',
        related_query_name="%(app_label)s_%(class)s_components_related_query", blank=True, help_text="all components of the reaction, also solvents, catalysts, ...")
    RInChI = models.TextField(blank=True, help_text="RInChi string")
    RInChI_key_short = models.TextField(
        blank=True, help_text="short RInChiKey key, s. link")
    RInChI_key_long = models.TextField(
        blank=True, help_text="long RInChiKey key")
    RInChI_web_key = models.TextField(
        blank=True, help_text="RInChiKey web key")
    RXNO = models.TextField(blank=True, help_text="RXNO")
    equilibrium_const = models.DecimalField(max_digits=20, decimal_places=9, blank=True, null=True,
                                            help_text="literature value of the equilibrium constant")
    deltaG = models.DecimalField(max_digits=20, decimal_places=9, blank=True, null=True,
                                 help_text="literature value of the free energy")
    order = models.IntegerField(null=True, blank=True,
                                help_text="order of the reaction in a set of multistep reactions, in case order plays a role. It does not describe the kinetics")
    safety_information = models.JSONField(
        blank=True, help_text="reaction safety information in human and machine readable form, including safety symbols in svg.")
    description = models.TextField(blank=True, null=True, help_text="")
    remarks = models.TextField(blank=True, null=True, help_text="")
    svg = models.TextField(blank=True, null=True,
                           help_text="svg image of the reaction")
    # remove hardcoded link: lara_substances/reactions/
    image = models.ImageField(upload_to='substances/reactions/', blank=True, null=True,
                              help_text="rel. path/filename to image")
    # TODO: add ManyToManyField for literature
    literature = models.URLField(
        null=True, blank=True, help_text="literature and references")
    # literature = models.ManyToManyField(
    #     LibItem, blank=True, help_text="literature and references")
    extra_data = models.ManyToManyField(ExtraData, related_name='%(app_label)s_%(class)s_extra_data_related',
                                        related_query_name="%(app_label)s_%(class)s_extra_data_related_query", blank=True,
                                        help_text="extra data, like reaction conditions, temperatures, equilibrium constants, energies, pH, yield  ...")

    def __str__(self):
        return self.name  # f"{self.RX_UUID}({self.name})" or ""


class MultiStepReaction(models.Model):
    """ multi step chemical reaction, like a multi step synthesis or an 
        enzymatic/biochemical pathway or enzymatic cascade reaction
        TODO: branched reactions / side reactions
    """

    # models.AutoField(primary_key=True)
    multi_reaction_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, help_text="reaction name")
    UUID = models.UUIDField(blank=True, null=True, unique=True, default=uuid.uuid4,
                            help_text="reaction UUID")
    IRI = models.URLField(
        blank=True,  null=True, unique=True, max_length=512, help_text="International Resource Identifier - IRI: is used for semantic representation ")
    reactions = models.ManyToManyField(
        Reaction, related_name='%(app_label)s_%(class)s_reactions_related',
        related_query_name="%(app_label)s_%(class)s_reactions_related_query", blank=True, help_text="all involved reactions")
    description = models.TextField(blank=True, null=True, help_text="")
    remarks = models.TextField(blank=True, null=True, help_text="")
    svg = models.TextField(blank=True, null=True,
                           help_text="svg image of the reaction")
    # remove hardcoded link: lara_substances/reactions/
    image = models.ImageField(upload_to='lara_substances/reactions/', blank=True, null=True,
                              help_text="rel. path/filename to image")
    # TODO: add ManyToManyField for literature
    literature = models.URLField(
        null=True, blank=True, help_text="literature and references")
    # literature = models.ManyToManyField(
    #     LibItem, blank=True, help_text="literature and references")
    extra_data = models.ManyToManyField(ExtraData, related_name='%(app_label)s_%(class)s_extra_data_related',
                                        related_query_name="%(app_label)s_%(class)s_extra_data_related_query", blank=True,
                                        help_text="extra data, like reaction conditions, temperatures")

    def __str__(self):
        return self.name  # f"{self.UUID}({self.name})" or ""


class SubstanceToOrder(models.Model):
    """
        this is used to mark substances and derived to be ordered
        for the ordering procedure in the related substance store ....
    """

    # models.AutoField(primary_key=True)
    subst2order_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    substance = models.ForeignKey(Substance, related_name='%(app_label)s_%(class)s_substances_related',
                                  related_query_name="%(app_label)s_%(class)s_substances_related_query",  on_delete=models.CASCADE, blank=True,
                                  help_text="substance to be ordered")
    polymer = models.ForeignKey(Polymer, related_name="polymer_2order", on_delete=models.CASCADE, blank=True,
                                help_text="polymer to be ordered")
    mix_component = models.ForeignKey(MixtureComponent, related_name='%(app_label)s_%(class)s_mix_components_related',
                                      related_query_name="%(app_label)s_%(class)s_mix_components_related_query",
                                      on_delete=models.CASCADE, blank=True, help_text="mixture component to be ordered")
    mixture = models.ForeignKey(Mixture, related_name='%(app_label)s_%(class)s_mixtures_related',
                                related_query_name="%(app_label)s_%(class)s_mixtures_related_query", on_delete=models.CASCADE, blank=True,
                                help_text="mixture to be ordered")
    user = models.ForeignKey(LaraUser, related_name='%(app_label)s_%(class)s_users_related',
                             related_query_name="%(app_label)s_%(class)s_users_related_query", on_delete=models.CASCADE, blank=True,
                             help_text="user who ordered the substance")
    datetime = models.DateTimeField(
        null=True, blank=True, help_text="date of labeling")

    def __str__(self):
        return self.substance.name or self.polymer.name or self.mixture.name or ""
