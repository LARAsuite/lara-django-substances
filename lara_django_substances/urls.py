"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_substances urls *

:details: lara_django_substances urls module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""

from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from django.views.generic import TemplateView
#import lara_django.urls as base_urls

from . import views

# Add your {cookiecutter.project_slug}} urls here.


# !! this sets the apps namespace to be used in the template
app_name = "lara_django_substances"

# companies and institutions should also be added
# the 'name' attribute is used in templates to address the url independent of the view

urlpatterns = [
    # the 'name' value as called by the {% url %} template tag
    path('substance/list/', views.SubstanceSingleTableView.as_view(), name='substance-list'),
    path('substance/create/', views.SubstanceCreateView.as_view(), name='substance-create'),
    path('substance/update/<uuid:pk>', views.SubstanceUpdateView.as_view(), name='substance-update'),
    path('substance/delete/<uuid:pk>', views.SubstanceDeleteView.as_view(), name='substance-delete'),
    path('substance/<uuid:pk>/', views.SubstanceDetailView.as_view(), name='substance-detail'),

    path('polymer/list/', views.PolymerSingleTableView.as_view(), name='polymer-list'),
    path('polymer/create/', views.PolymerCreateView.as_view(), name='polymer-create'),
    path('polymer/update/<uuid:pk>', views.PolymerUpdateView.as_view(), name='polymer-update'),
    path('polymer/delete/<uuid:pk>', views.PolymerDeleteView.as_view(), name='polymer-delete'),
    path('polymer/<uuid:pk>/', views.PolymerDetailView.as_view(), name='polymer-detail'),

    path('mixture/list/', views.MixtureSingleTableView.as_view(), name='mixture-list'),
    path('mixture/create/', views.MixtureCreateView.as_view(), name='mixture-create'),
    path('mixture/update/<uuid:pk>', views.MixtureUpdateView.as_view(), name='mixture-update'),
    path('mixture/delete/<uuid:pk>', views.MixtureDeleteView.as_view(), name='mixture-delete'),
    path('mixture/<uuid:pk>/', views.MixtureDetailView.as_view(), name='mixture-detail'),
    #path('', views.IndexView.as_view(), name='index'),
    #path('browse/', views.browse, name='substances-browser'),
    #path('<int:pk>/', views.SubstanceDetail.as_view(), name='substance-detail'),
    #path('substanceslist/', views.SubstancesListView.as_view(), name='substances-list'),
    #path('add/', views.SubstanceCreateView.as_view(), name='substance-create'),
    #~ path('(?P<pk>[0-9]+)/', views.SubstanceDetail.as_view(), name='substance-detail'),
    #path('search', views.SubstanceSearchView.as_view(), name='search'),
    #path('search-results/', views.SubstanceSearchResults.as_view(), name='search-results'),
    #path('results/', views.results, name='results'),
    #~ path(r'^(?P<question_id>[0-9]+)/$', views.detail, name='detail'),
    #~ path(r'^(?P<question_id>[0-9]+)/results/$', views.results, name='results'),
    #~ path(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
    path('', views.SubstanceSingleTableView.as_view(), name='substances-root'),
]


# urlpatterns = [
#     path('', views.EntitySingleTableView.as_view(), name='entity-list-root'),
#     path('entities/list/', views.EntitySingleTableView.as_view(), name='entity-list'),
#     path('addresses/list/', views.AddressesListView.as_view(), name='address-list'),
#     path('addresses/create/', views.AddressCreateView.as_view(),
#          name='address-create'),
#     path('addresses/update/<uuid:pk>', views.AddressUpdateView.as_view(),
#          name='address-update'),
#     path('addresses/delete/<uuid:pk>', views.AddressDeleteView.as_view(),
#          name='address-delete'),
# ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
