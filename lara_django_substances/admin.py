"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_substances admin *

:details: lara_django_substances admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev admin_generator lara_django_substances >> admin.py" to update this file
________________________________________________________________________
"""
# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import ExtraData, SubstanceClass, PhysicalState, Isotope, Substance, Polymer, MixtureComponent, Mixture, MoleculeResidues, Reactand, Reaction, MultiStepReaction, SubstanceToOrder


@admin.register(ExtraData)
class ExtraDataAdmin(admin.ModelAdmin):
    list_display = (
        'data_type',
        'namespace',
        'URI',
        'text',
        'XML',
        'JSON',
        'bin',
        'media_type',
        'IRI',
        'URL',
        'description',
        'extra_data_id',
        'file',
        'image',
    )
    list_filter = ('data_type', 'namespace', 'media_type')


@admin.register(SubstanceClass)
class SubstanceClassAdmin(admin.ModelAdmin):
    list_display = ('class_id', 'substance_class', 'description')


@admin.register(PhysicalState)
class PhysicalStateAdmin(admin.ModelAdmin):
    list_display = ('state_id', 'state', 'description')


@admin.register(Isotope)
class IsotopeAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'acronym',
        'synonyms',
        'UUID',
        'IRI',
        'CAS',
        'IUPAC_name',
        'PubChemID',
        'PubChemFingerprint',
        'ChemSpiderID',
        'BeilsteinRegNum',
        'EC_num',
        'MDL_num',
        'PDB_code',
        'computed_properties',
        'properties',
        'sumformula',
        'CML',
        'CDXML',
        'SMILES',
        'IsoSMILES',
        'CanSMILES',
        'InChI',
        'InChIKey',
        'StdInChI',
        'StdInChIKey',
        'PDB',
        'PDB_URL',
        'URL',
        'URL_definition',
        'LARA_structure_URL',
        'mol_raw',
        'mol_2D',
        'mol_3D',
        'svg',
        'image',
        'literature',
        'remarks',
        'description',
        'isotope_id',
        'num_protons',
        'electron_config',
    )
    raw_id_fields = ('substance_class', 'extra_data', 'tags')
    search_fields = ('name',)


@admin.register(Substance)
class SubstanceAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'acronym',
        'synonyms',
        'UUID',
        'IRI',
        'CAS',
        'IUPAC_name',
        'PubChemID',
        'PubChemFingerprint',
        'ChemSpiderID',
        'BeilsteinRegNum',
        'EC_num',
        'MDL_num',
        'PDB_code',
        'computed_properties',
        'properties',
        'sumformula',
        'CML',
        'CDXML',
        'SMILES',
        'IsoSMILES',
        'CanSMILES',
        'InChI',
        'InChIKey',
        'StdInChI',
        'StdInChIKey',
        'PDB',
        'PDB_URL',
        'URL',
        'URL_definition',
        'LARA_structure_URL',
        'mol_raw',
        'mol_2D',
        'mol_3D',
        'svg',
        'image',
        'literature',
        'remarks',
        'description',
        'substance_id',
        'm1_code',
        'm3_code',
        'm5_code',
        'mwt',
        'density',
        'AlogP',
        'XlogP',
        'melting_point',
        'boiling_point',
    )
    raw_id_fields = ('substance_class', 'extra_data', 'tags')
    search_fields = ('name',)


@admin.register(Polymer)
class PolymerAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'acronym',
        'synonyms',
        'UUID',
        'IRI',
        'CAS',
        'IUPAC_name',
        'PubChemID',
        'PubChemFingerprint',
        'ChemSpiderID',
        'BeilsteinRegNum',
        'EC_num',
        'MDL_num',
        'PDB_code',
        'computed_properties',
        'properties',
        'sumformula',
        'CML',
        'CDXML',
        'SMILES',
        'IsoSMILES',
        'CanSMILES',
        'InChI',
        'InChIKey',
        'StdInChI',
        'StdInChIKey',
        'PDB',
        'PDB_URL',
        'URL',
        'URL_definition',
        'LARA_structure_URL',
        'mol_raw',
        'mol_2D',
        'mol_3D',
        'svg',
        'image',
        'literature',
        'remarks',
        'description',
        'polymers_id',
        'sequence',
        'melting_point',
        'boiling_point',
        'media_type',
        'sequence_file',
    )
    list_filter = ('sequence', 'media_type')
    raw_id_fields = ('substance_class', 'extra_data', 'tags')
    search_fields = ('name',)


@admin.register(MixtureComponent)
class MixtureComponentAdmin(admin.ModelAdmin):
    list_display = (
        'mix_comp_id',
        'name',
        'IRI',
        'substance',
        'polymer',
        'concentration',
        'concentration_weight',
        'description',
    )
    list_filter = ('substance', 'polymer')
    search_fields = ('name',)


@admin.register(Mixture)
class MixtureAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'acronym',
        'synonyms',
        'UUID',
        'IRI',
        'CAS',
        'IUPAC_name',
        'PubChemID',
        'PubChemFingerprint',
        'ChemSpiderID',
        'BeilsteinRegNum',
        'EC_num',
        'MDL_num',
        'PDB_code',
        'computed_properties',
        'properties',
        'sumformula',
        'CML',
        'CDXML',
        'SMILES',
        'IsoSMILES',
        'CanSMILES',
        'InChI',
        'InChIKey',
        'StdInChI',
        'StdInChIKey',
        'PDB',
        'PDB_URL',
        'URL',
        'URL_definition',
        'LARA_structure_URL',
        'mol_raw',
        'mol_2D',
        'mol_3D',
        'svg',
        'image',
        'literature',
        'remarks',
        'description',
        'mixture_id',
    )
    raw_id_fields = ('substance_class', 'extra_data', 'tags', 'components')
    search_fields = ('name',)


@admin.register(MoleculeResidues)
class MoleculeResiduesAdmin(admin.ModelAdmin):
    list_display = ('residue_id', 'name', 'substructure', 'IRI')
    search_fields = ('name',)


@admin.register(Reactand)
class ReactandAdmin(admin.ModelAdmin):
    list_display = (
        'reactand_id',
        'IRI',
        'substance',
        'polymer',
        'mixture',
        'order',
        'stoichiometric_factor',
        'physical_state',
        'literature',
    )
    list_filter = ('substance', 'polymer', 'mixture', 'physical_state')
    raw_id_fields = ('role',)


@admin.register(Reaction)
class ReactionAdmin(admin.ModelAdmin):
    list_display = (
        'reaction_id',
        'name',
        'label',
        'RX_UUID',
        'IRI',
        'RInChI',
        'RInChI_key_short',
        'RInChI_key_long',
        'RInChI_web_key',
        'RXNO',
        'equilibrium_const',
        'deltaG',
        'order',
        'safety_information',
        'description',
        'remarks',
        'svg',
        'image',
        'literature',
    )
    raw_id_fields = ('components', 'extra_data')
    search_fields = ('name',)


@admin.register(MultiStepReaction)
class MultiStepReactionAdmin(admin.ModelAdmin):
    list_display = (
        'multi_reaction_id',
        'name',
        'UUID',
        'IRI',
        'description',
        'remarks',
        'svg',
        'image',
        'literature',
    )
    raw_id_fields = ('reactions', 'extra_data')
    search_fields = ('name',)


@admin.register(SubstanceToOrder)
class SubstanceToOrderAdmin(admin.ModelAdmin):
    list_display = (
        'subst2order_id',
        'substance',
        'polymer',
        'mix_component',
        'mixture',
        'user',
        'datetime',
    )
    list_filter = (
        'substance',
        'polymer',
        'mix_component',
        'mixture',
        'user',
        'datetime',
    )
