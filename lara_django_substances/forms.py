"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_substances admin *

:details: lara_django_substances admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev forms_generator -c lara_django_substances > forms.py" to update this file
________________________________________________________________________
"""
# django crispy forms s. https://github.com/django-crispy-forms/django-crispy-forms for []
# generated with django-extensions forms_generator -c  [] > forms.py (LARA-version)

from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column

from .models import ExtraData, SubstanceClass, PhysicalState, Isotope, Substance, Polymer, MixtureComponent, Mixture, MoleculeResidues, Reactand, Reaction, MultiStepReaction, SubstanceToOrder


class SearchForm(forms.Form):
    name = forms.CharField()

    def search(self):
        # search
        pass


class ExtraDataCreateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = (
            'data_type',
            'namespace',
            'URI',
            'text',
            'XML',
            'JSON',
            'media_type',
            'IRI',
            'URL',
            'description',
            'file',
            'image')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'data_type',
            'namespace',
            'URI',
            'text',
            'XML',
            'JSON',
            'media_type',
            'IRI',
            'URL',
            'description',
            'file',
            'image',
            Submit('submit', 'Create')
        )


class ExtraDataUpdateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = (
            'data_type',
            'namespace',
            'URI',
            'text',
            'XML',
            'JSON',
            'media_type',
            'IRI',
            'URL',
            'description',
            'file',
            'image')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'data_type',
            'namespace',
            'URI',
            'text',
            'XML',
            'JSON',
            'media_type',
            'IRI',
            'URL',
            'description',
            'file',
            'image',
            Submit('submit', 'Create')
        )


class SubstanceClassCreateForm(forms.ModelForm):
    class Meta:
        model = SubstanceClass
        fields = (
            'substance_class',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'substance_class',
            'description',
            Submit('submit', 'Create')
        )


class SubstanceClassUpdateForm(forms.ModelForm):
    class Meta:
        model = SubstanceClass
        fields = (
            'substance_class',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'substance_class',
            'description',
            Submit('submit', 'Create')
        )


class PhysicalStateCreateForm(forms.ModelForm):
    class Meta:
        model = PhysicalState
        fields = (
            'state',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'state',
            'description',
            Submit('submit', 'Create')
        )


class PhysicalStateUpdateForm(forms.ModelForm):
    class Meta:
        model = PhysicalState
        fields = (
            'state',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'state',
            'description',
            Submit('submit', 'Create')
        )


class IsotopeCreateForm(forms.ModelForm):
    class Meta:
        model = Isotope
        fields = (
            'name',
            'acronym',
            'synonyms',
            'UUID',
            'IRI',
            'CAS',
            'IUPAC_name',
            'PubChemID',
            'PubChemFingerprint',
            'ChemSpiderID',
            'BeilsteinRegNum',
            'EC_num',
            'MDL_num',
            'PDB_code',
            'computed_properties',
            'properties',
            'sumformula',
            'CML',
            'CDXML',
            'SMILES',
            'IsoSMILES',
            'CanSMILES',
            'InChI',
            'InChIKey',
            'StdInChI',
            'StdInChIKey',
            'PDB',
            'PDB_URL',
            'URL',
            'URL_definition',
            'LARA_structure_URL',
            'mol_raw',
            'mol_2D',
            'mol_3D',
            'svg',
            'image',
            'literature',
            'remarks',
            'description',
            'num_protons',
            'electron_config')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'acronym',
            'synonyms',
            'UUID',
            'IRI',
            'CAS',
            'IUPAC_name',
            'PubChemID',
            'PubChemFingerprint',
            'ChemSpiderID',
            'BeilsteinRegNum',
            'EC_num',
            'MDL_num',
            'PDB_code',
            'computed_properties',
            'properties',
            'sumformula',
            'CML',
            'CDXML',
            'SMILES',
            'IsoSMILES',
            'CanSMILES',
            'InChI',
            'InChIKey',
            'StdInChI',
            'StdInChIKey',
            'PDB',
            'PDB_URL',
            'URL',
            'URL_definition',
            'LARA_structure_URL',
            'mol_raw',
            'mol_2D',
            'mol_3D',
            'svg',
            'image',
            'literature',
            'remarks',
            'description',
            'num_protons',
            'electron_config',
            Submit('submit', 'Create')
        )


class IsotopeUpdateForm(forms.ModelForm):
    class Meta:
        model = Isotope
        fields = (
            'name',
            'acronym',
            'synonyms',
            'UUID',
            'IRI',
            'CAS',
            'IUPAC_name',
            'PubChemID',
            'PubChemFingerprint',
            'ChemSpiderID',
            'BeilsteinRegNum',
            'EC_num',
            'MDL_num',
            'PDB_code',
            'computed_properties',
            'properties',
            'sumformula',
            'CML',
            'CDXML',
            'SMILES',
            'IsoSMILES',
            'CanSMILES',
            'InChI',
            'InChIKey',
            'StdInChI',
            'StdInChIKey',
            'PDB',
            'PDB_URL',
            'URL',
            'URL_definition',
            'LARA_structure_URL',
            'mol_raw',
            'mol_2D',
            'mol_3D',
            'svg',
            'image',
            'literature',
            'remarks',
            'description',
            'num_protons',
            'electron_config')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'acronym',
            'synonyms',
            'UUID',
            'IRI',
            'CAS',
            'IUPAC_name',
            'PubChemID',
            'PubChemFingerprint',
            'ChemSpiderID',
            'BeilsteinRegNum',
            'EC_num',
            'MDL_num',
            'PDB_code',
            'computed_properties',
            'properties',
            'sumformula',
            'CML',
            'CDXML',
            'SMILES',
            'IsoSMILES',
            'CanSMILES',
            'InChI',
            'InChIKey',
            'StdInChI',
            'StdInChIKey',
            'PDB',
            'PDB_URL',
            'URL',
            'URL_definition',
            'LARA_structure_URL',
            'mol_raw',
            'mol_2D',
            'mol_3D',
            'svg',
            'image',
            'literature',
            'remarks',
            'description',
            'num_protons',
            'electron_config',
            Submit('submit', 'Update')
        )


class SubstanceCreateForm(forms.ModelForm):
    class Meta:
        model = Substance
        fields = (
            'name',
            'acronym',
            'synonyms',
            'UUID',
            'IRI',
            'CAS',
            'IUPAC_name',
            'PubChemID',
            'PubChemFingerprint',
            'ChemSpiderID',
            'BeilsteinRegNum',
            'EC_num',
            'MDL_num',
            'PDB_code',
            'computed_properties',
            'properties',
            'sumformula',
            'CML',
            'CDXML',
            'SMILES',
            'IsoSMILES',
            'CanSMILES',
            'InChI',
            'InChIKey',
            'StdInChI',
            'StdInChIKey',
            'PDB',
            'PDB_URL',
            'URL',
            'URL_definition',
            'LARA_structure_URL',
            'mol_raw',
            'mol_2D',
            'mol_3D',
            'svg',
            'image',
            'literature',
            'remarks',
            'description',
            'm1_code',
            'm3_code',
            'm5_code',
            'mwt',
            'density',
            'AlogP',
            'XlogP',
            'melting_point',
            'boiling_point')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'acronym',
            'synonyms',
            'UUID',
            'IRI',
            'CAS',
            'IUPAC_name',
            'PubChemID',
            'PubChemFingerprint',
            'ChemSpiderID',
            'BeilsteinRegNum',
            'EC_num',
            'MDL_num',
            'PDB_code',
            'computed_properties',
            'properties',
            'sumformula',
            'CML',
            'CDXML',
            'SMILES',
            'IsoSMILES',
            'CanSMILES',
            'InChI',
            'InChIKey',
            'StdInChI',
            'StdInChIKey',
            'PDB',
            'PDB_URL',
            'URL',
            'URL_definition',
            'LARA_structure_URL',
            'mol_raw',
            'mol_2D',
            'mol_3D',
            'svg',
            'image',
            'literature',
            'remarks',
            'description',
            'm1_code',
            'm3_code',
            'm5_code',
            'mwt',
            'density',
            'AlogP',
            'XlogP',
            'melting_point',
            'boiling_point',
            Submit('submit', 'Create')
        )


class SubstanceUpdateForm(forms.ModelForm):
    class Meta:
        model = Substance
        fields = (
            'name',
            'acronym',
            'synonyms',
            'UUID',
            'IRI',
            'CAS',
            'IUPAC_name',
            'PubChemID',
            'PubChemFingerprint',
            'ChemSpiderID',
            'BeilsteinRegNum',
            'EC_num',
            'MDL_num',
            'PDB_code',
            'computed_properties',
            'properties',
            'sumformula',
            'CML',
            'CDXML',
            'SMILES',
            'IsoSMILES',
            'CanSMILES',
            'InChI',
            'InChIKey',
            'StdInChI',
            'StdInChIKey',
            'PDB',
            'PDB_URL',
            'URL',
            'URL_definition',
            'LARA_structure_URL',
            'mol_raw',
            'mol_2D',
            'mol_3D',
            'svg',
            'image',
            'literature',
            'remarks',
            'description',
            'm1_code',
            'm3_code',
            'm5_code',
            'mwt',
            'density',
            'AlogP',
            'XlogP',
            'melting_point',
            'boiling_point')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'acronym',
            'synonyms',
            'UUID',
            'IRI',
            'CAS',
            'IUPAC_name',
            'PubChemID',
            'PubChemFingerprint',
            'ChemSpiderID',
            'BeilsteinRegNum',
            'EC_num',
            'MDL_num',
            'PDB_code',
            'computed_properties',
            'properties',
            'sumformula',
            'CML',
            'CDXML',
            'SMILES',
            'IsoSMILES',
            'CanSMILES',
            'InChI',
            'InChIKey',
            'StdInChI',
            'StdInChIKey',
            'PDB',
            'PDB_URL',
            'URL',
            'URL_definition',
            'LARA_structure_URL',
            'mol_raw',
            'mol_2D',
            'mol_3D',
            'svg',
            'image',
            'literature',
            'remarks',
            'description',
            'm1_code',
            'm3_code',
            'm5_code',
            'mwt',
            'density',
            'AlogP',
            'XlogP',
            'melting_point',
            'boiling_point',
            Submit('submit', 'Update')
        )


class PolymerCreateForm(forms.ModelForm):
    class Meta:
        model = Polymer
        fields = (
            'name',
            'acronym',
            'synonyms',
            'UUID',
            'IRI',
            'CAS',
            'IUPAC_name',
            'PubChemID',
            'PubChemFingerprint',
            'ChemSpiderID',
            'BeilsteinRegNum',
            'EC_num',
            'MDL_num',
            'PDB_code',
            'computed_properties',
            'properties',
            'sumformula',
            'CML',
            'CDXML',
            'SMILES',
            'IsoSMILES',
            'CanSMILES',
            'InChI',
            'InChIKey',
            'StdInChI',
            'StdInChIKey',
            'PDB',
            'PDB_URL',
            'URL',
            'URL_definition',
            'LARA_structure_URL',
            'mol_raw',
            'mol_2D',
            'mol_3D',
            'svg',
            'image',
            'literature',
            'remarks',
            'description',
            'sequence',
            'melting_point',
            'boiling_point',
            'media_type',
            'sequence_file')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'acronym',
            'synonyms',
            'UUID',
            'IRI',
            'CAS',
            'IUPAC_name',
            'PubChemID',
            'PubChemFingerprint',
            'ChemSpiderID',
            'BeilsteinRegNum',
            'EC_num',
            'MDL_num',
            'PDB_code',
            'computed_properties',
            'properties',
            'sumformula',
            'CML',
            'CDXML',
            'SMILES',
            'IsoSMILES',
            'CanSMILES',
            'InChI',
            'InChIKey',
            'StdInChI',
            'StdInChIKey',
            'PDB',
            'PDB_URL',
            'URL',
            'URL_definition',
            'LARA_structure_URL',
            'mol_raw',
            'mol_2D',
            'mol_3D',
            'svg',
            'image',
            'literature',
            'remarks',
            'description',
            'sequence',
            'melting_point',
            'boiling_point',
            'media_type',
            'sequence_file',
            Submit('submit', 'Create')
        )


class PolymerUpdateForm(forms.ModelForm):
    class Meta:
        model = Polymer
        fields = (
            'name',
            'acronym',
            'synonyms',
            'UUID',
            'IRI',
            'CAS',
            'IUPAC_name',
            'PubChemID',
            'PubChemFingerprint',
            'ChemSpiderID',
            'BeilsteinRegNum',
            'EC_num',
            'MDL_num',
            'PDB_code',
            'computed_properties',
            'properties',
            'sumformula',
            'CML',
            'CDXML',
            'SMILES',
            'IsoSMILES',
            'CanSMILES',
            'InChI',
            'InChIKey',
            'StdInChI',
            'StdInChIKey',
            'PDB',
            'PDB_URL',
            'URL',
            'URL_definition',
            'LARA_structure_URL',
            'mol_raw',
            'mol_2D',
            'mol_3D',
            'svg',
            'image',
            'literature',
            'remarks',
            'description',
            'sequence',
            'melting_point',
            'boiling_point',
            'media_type',
            'sequence_file')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'acronym',
            'synonyms',
            'UUID',
            'IRI',
            'CAS',
            'IUPAC_name',
            'PubChemID',
            'PubChemFingerprint',
            'ChemSpiderID',
            'BeilsteinRegNum',
            'EC_num',
            'MDL_num',
            'PDB_code',
            'computed_properties',
            'properties',
            'sumformula',
            'CML',
            'CDXML',
            'SMILES',
            'IsoSMILES',
            'CanSMILES',
            'InChI',
            'InChIKey',
            'StdInChI',
            'StdInChIKey',
            'PDB',
            'PDB_URL',
            'URL',
            'URL_definition',
            'LARA_structure_URL',
            'mol_raw',
            'mol_2D',
            'mol_3D',
            'svg',
            'image',
            'literature',
            'remarks',
            'description',
            'sequence',
            'melting_point',
            'boiling_point',
            'media_type',
            'sequence_file',
            Submit('submit', 'Update')
        )


class MixtureComponentCreateForm(forms.ModelForm):
    class Meta:
        model = MixtureComponent
        fields = (
            'name',
            'IRI',
            'substance',
            'polymer',
            'concentration',
            'concentration_weight',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'IRI',
            'substance',
            'polymer',
            'concentration',
            'concentration_weight',
            'description',
            Submit('submit', 'Create')
        )


class MixtureComponentUpdateForm(forms.ModelForm):
    class Meta:
        model = MixtureComponent
        fields = (
            'name',
            'IRI',
            'substance',
            'polymer',
            'concentration',
            'concentration_weight',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'IRI',
            'substance',
            'polymer',
            'concentration',
            'concentration_weight',
            'description',
            Submit('submit', 'Create')
        )


class MixtureCreateForm(forms.ModelForm):
    class Meta:
        model = Mixture
        fields = (
            'name',
            'acronym',
            'synonyms',
            'UUID',
            'IRI',
            'CAS',
            'IUPAC_name',
            'PubChemID',
            'PubChemFingerprint',
            'ChemSpiderID',
            'BeilsteinRegNum',
            'EC_num',
            'MDL_num',
            'PDB_code',
            'computed_properties',
            'properties',
            'sumformula',
            'CML',
            'CDXML',
            'SMILES',
            'IsoSMILES',
            'CanSMILES',
            'InChI',
            'InChIKey',
            'StdInChI',
            'StdInChIKey',
            'PDB',
            'PDB_URL',
            'URL',
            'URL_definition',
            'LARA_structure_URL',
            'mol_raw',
            'mol_2D',
            'mol_3D',
            'svg',
            'image',
            'literature',
            'remarks',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'acronym',
            'synonyms',
            'UUID',
            'IRI',
            'CAS',
            'IUPAC_name',
            'PubChemID',
            'PubChemFingerprint',
            'ChemSpiderID',
            'BeilsteinRegNum',
            'EC_num',
            'MDL_num',
            'PDB_code',
            'computed_properties',
            'properties',
            'sumformula',
            'CML',
            'CDXML',
            'SMILES',
            'IsoSMILES',
            'CanSMILES',
            'InChI',
            'InChIKey',
            'StdInChI',
            'StdInChIKey',
            'PDB',
            'PDB_URL',
            'URL',
            'URL_definition',
            'LARA_structure_URL',
            'mol_raw',
            'mol_2D',
            'mol_3D',
            'svg',
            'image',
            'literature',
            'remarks',
            'description',
            Submit('submit', 'Create')
        )


class MixtureUpdateForm(forms.ModelForm):
    class Meta:
        model = Mixture
        fields = (
            'name',
            'acronym',
            'synonyms',
            'UUID',
            'IRI',
            'CAS',
            'IUPAC_name',
            'PubChemID',
            'PubChemFingerprint',
            'ChemSpiderID',
            'BeilsteinRegNum',
            'EC_num',
            'MDL_num',
            'PDB_code',
            'computed_properties',
            'properties',
            'sumformula',
            'CML',
            'CDXML',
            'SMILES',
            'IsoSMILES',
            'CanSMILES',
            'InChI',
            'InChIKey',
            'StdInChI',
            'StdInChIKey',
            'PDB',
            'PDB_URL',
            'URL',
            'URL_definition',
            'LARA_structure_URL',
            'mol_raw',
            'mol_2D',
            'mol_3D',
            'svg',
            'image',
            'literature',
            'remarks',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'acronym',
            'synonyms',
            'UUID',
            'IRI',
            'CAS',
            'IUPAC_name',
            'PubChemID',
            'PubChemFingerprint',
            'ChemSpiderID',
            'BeilsteinRegNum',
            'EC_num',
            'MDL_num',
            'PDB_code',
            'computed_properties',
            'properties',
            'sumformula',
            'CML',
            'CDXML',
            'SMILES',
            'IsoSMILES',
            'CanSMILES',
            'InChI',
            'InChIKey',
            'StdInChI',
            'StdInChIKey',
            'PDB',
            'PDB_URL',
            'URL',
            'URL_definition',
            'LARA_structure_URL',
            'mol_raw',
            'mol_2D',
            'mol_3D',
            'svg',
            'image',
            'literature',
            'remarks',
            'description',
            Submit('submit', 'Update')
        )


class MoleculeResiduesCreateForm(forms.ModelForm):
    class Meta:
        model = MoleculeResidues
        fields = (
            'name',
            'substructure',
            'IRI')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'substructure',
            'IRI',
            Submit('submit', 'Create')
        )


class MoleculeResiduesUpdateForm(forms.ModelForm):
    class Meta:
        model = MoleculeResidues
        fields = (
            'name',
            'substructure',
            'IRI')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'substructure',
            'IRI',
            Submit('submit', 'Create')
        )


class ReactandCreateForm(forms.ModelForm):
    class Meta:
        model = Reactand
        fields = (
            'IRI',
            'substance',
            'polymer',
            'mixture',
            'order',
            'stoichiometric_factor',
            'physical_state',
            'literature')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'IRI',
            'substance',
            'polymer',
            'mixture',
            'order',
            'stoichiometric_factor',
            'physical_state',
            'literature',
            Submit('submit', 'Create')
        )


class ReactandUpdateForm(forms.ModelForm):
    class Meta:
        model = Reactand
        fields = (
            'IRI',
            'substance',
            'polymer',
            'mixture',
            'order',
            'stoichiometric_factor',
            'physical_state',
            'literature')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'IRI',
            'substance',
            'polymer',
            'mixture',
            'order',
            'stoichiometric_factor',
            'physical_state',
            'literature',
            Submit('submit', 'Create')
        )


class ReactionCreateForm(forms.ModelForm):
    class Meta:
        model = Reaction
        fields = (
            'name',
            'label',
            'RX_UUID',
            'IRI',
            'RInChI',
            'RInChI_key_short',
            'RInChI_key_long',
            'RInChI_web_key',
            'RXNO',
            'equilibrium_const',
            'deltaG',
            'order',
            'safety_information',
            'description',
            'remarks',
            'svg',
            'image',
            'literature')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'label',
            'RX_UUID',
            'IRI',
            'RInChI',
            'RInChI_key_short',
            'RInChI_key_long',
            'RInChI_web_key',
            'RXNO',
            'equilibrium_const',
            'deltaG',
            'order',
            'safety_information',
            'description',
            'remarks',
            'svg',
            'image',
            'literature',
            Submit('submit', 'Create')
        )


class ReactionUpdateForm(forms.ModelForm):
    class Meta:
        model = Reaction
        fields = (
            'name',
            'label',
            'RX_UUID',
            'IRI',
            'RInChI',
            'RInChI_key_short',
            'RInChI_key_long',
            'RInChI_web_key',
            'RXNO',
            'equilibrium_const',
            'deltaG',
            'order',
            'safety_information',
            'description',
            'remarks',
            'svg',
            'image',
            'literature')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'label',
            'RX_UUID',
            'IRI',
            'RInChI',
            'RInChI_key_short',
            'RInChI_key_long',
            'RInChI_web_key',
            'RXNO',
            'equilibrium_const',
            'deltaG',
            'order',
            'safety_information',
            'description',
            'remarks',
            'svg',
            'image',
            'literature',
            Submit('submit', 'Update')
        )


class MultiStepReactionCreateForm(forms.ModelForm):
    class Meta:
        model = MultiStepReaction
        fields = (
            'name',
            'UUID',
            'IRI',
            'description',
            'remarks',
            'svg',
            'image',
            'literature')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'UUID',
            'IRI',
            'description',
            'remarks',
            'svg',
            'image',
            'literature',
            Submit('submit', 'Create')
        )


class MultiStepReactionUpdateForm(forms.ModelForm):
    class Meta:
        model = MultiStepReaction
        fields = (
            'name',
            'UUID',
            'IRI',
            'description',
            'remarks',
            'svg',
            'image',
            'literature')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'UUID',
            'IRI',
            'description',
            'remarks',
            'svg',
            'image',
            'literature',
            Submit('submit', 'Create')
        )


class SubstanceToOrderCreateForm(forms.ModelForm):
    class Meta:
        model = SubstanceToOrder
        fields = (
            'substance',
            'polymer',
            'mix_component',
            'mixture',
            'user',
            'datetime')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'substance',
            'polymer',
            'mix_component',
            'mixture',
            'user',
            'datetime',
            Submit('submit', 'Create')
        )


class SubstanceToOrderUpdateForm(forms.ModelForm):
    class Meta:
        model = SubstanceToOrder
        fields = (
            'substance',
            'polymer',
            'mix_component',
            'mixture',
            'user',
            'datetime')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'substance',
            'polymer',
            'mix_component',
            'mixture',
            'user',
            'datetime',
            Submit('submit', 'Update')
        )

# from .forms import ExtraDataCreateForm, SubstanceClassCreateForm, PhysicalStateCreateForm, IsotopeCreateForm, SubstanceCreateForm, PolymerCreateForm, MixtureComponentCreateForm, MixtureCreateForm, MoleculeResiduesCreateForm, ReactandCreateForm, ReactionCreateForm, MultiStepReactionCreateForm, SubstanceToOrderCreateFormExtraDataUpdateForm, SubstanceClassUpdateForm, PhysicalStateUpdateForm, IsotopeUpdateForm, SubstanceUpdateForm, PolymerUpdateForm, MixtureComponentUpdateForm, MixtureUpdateForm, MoleculeResiduesUpdateForm, ReactandUpdateForm, ReactionUpdateForm, MultiStepReactionUpdateForm, SubstanceToOrderUpdateForm
