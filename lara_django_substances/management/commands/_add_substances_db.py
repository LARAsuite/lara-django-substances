"""
________________________________________________________________________

:PROJECT: LARA

*Add substances to lara substance database*

:details: Add substances to lara substance database
         -

:file:    add_substances.py
:authors: mark doerr (mark@ismeralda.org)

:date: (creation)        20200417 

.. note:: -
.. todo:: -
________________________________________________________________________
"""
__version__ = "0.2.1"

import os
import sys
import shutil
import re
import logging
import subprocess
import json
import csv
from collections import defaultdict

import multiprocessing
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor

from django.conf import settings
from django.db import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist

#import laralib.chem.substance_prop_calc as spc

import laralib.chem.merck_sigma_aldrich as msa
from laralib.chem.pubchem import SubstanceReaderPubChem

from lara_django_base.models import Namespace, Location
from lara_django_people.models import Entity
from lara_django_substances.models import Substance, Polymer, Mixture
from lara_django_substances_store.models import SubstanceInstance, PolymerInstance, MixtureInstance

class AddSubstances2DB(BaseCommand):
    """Adding substances to django database, could be later expanded to other file formats,
       in this case, the file opening should be moved to the processing unit """
    def __init__(self, event=None, replace_data=False):
        super(AddSubstances2DB, self).__init__()

        self.replace_data = replace_data

        if self.replace_data:
            logging.warning("Replacing data in db !!!")

        if event :
            self.file_to_processed_name = event.name
            self.file_to_processed_full_name = os.path.join(event.path, event.name)
    
    def addSubstancesCSV(self, csv_filename: str = "substances.csv") -> None:
        """adding substances using information from CSV file.
        This method should be used, if vendor and product numbers are known.

        The following columns should be present:

        :param csv_filename: defaults to "substances.csv"
        :type csv_filename: str, optional
        """

        # for statistics
        vendors = defaultdict(int)
        locations = defaultdict(int)
        rows_num = 0

        workers = multiprocessing.cpu_count() # get num cores

        logging.info(f"Multithreading, using {workers} CPUs ")
        
        try:
            with open(csv_filename) as csvfile:
                self.csv_reader = csv.DictReader(csvfile)
                self.stdout.write(self.style.SUCCESS(f"now adding substances from csv file: [{csv_filename}]..." ) )

                with ThreadPoolExecutor(workers) as ex:
                    res = ex.map(self._parse_substance, self.csv_reader)
                    return list(res) # important for catching error

        except ValueError as err:
            self.stderr.write(f'file {csv_filename}, ValueError: {err}')
            exit()
        except KeyError as err:
            self.stderr.write(f'file {csv_filename}, KeyError: {err}\n Please check, if file is exported with quote delimited strings')
            exit()
        except Exception as err:
            self.stderr.write(f'file {csv_filename}, GeneralError - addSubstancesCSV: {err}')
            exit()

        self.stdout.write(self.style.SUCCESS("All substances added ! Have a nice day ;)"))

    def addSubstancesDB(self, csv_filename_db: str = "") -> None:
        """adding substances using information from databases, like
           PubChem, Chemspider, wikipedia, ...
           A csv file provides the names or CAS numbers...
        """
        try:
            with open(csv_filename_db) as csvfile:
                self.csv_reader = csv.DictReader(csvfile)
                self.stdout.write(self.style.SUCCESS("now adding substances from databases - substance list: [{}]...".format(csv_filename_db) ) )

                for substance_row in self.csv_reader:
                    logging.debug(f"----------- {substance_row}")
                    logging.debug(substance_row['substance_name'])
                    #~ logging.debug(substance_row['CAS'])

                    srpc = SubstanceReaderPubChem()
                    substances, substances_raw = srpc.pubChemCompoundByName(substance_name=substance_row['substance_name'])

                    for substance in substances:
                        logging.debug("creating {}".format(substance) )
                        ## !! check first, if exists...
                        Substance.objects.create(**substance)

                    #~ ci_subst = ChemInfo()

                    #~ acronym = substance_row['acronym']

                    #~ ci_subst.pubChemCompoundByName(substance_name=substance_row['CAS'], acronym='')

                    self.stdout.write(self.style.SUCCESS("substance {} added !".format(substance_row['substance_name']) ))

                    #~ for subst in ci_subst.substancesList() :
                        #~ logging.debug("subst{}".format(subst) )
                        #~ s = Substance(**subst)
                        #~ s.save() # try bulk save

        except ValueError as err:
            self.stderr.write('file {}, ValueError: {}'.format(csv_filename_db, err))
            exit()
        except KeyError as err:
            self.stderr.write('file {}, KeyError: {}\n Please check, if file is exported with quote delimited strings'.format(csv_filename_db, err))
            exit()
        except Exception as err:
            self.stderr.write('file {}, Error: {}'.format(csv_filename_db, err))
            exit()

        self.stdout.write(self.style.SUCCESS("All substances added ! Have a nice day ;)"))

    def _parse_substance(self, substance_row):
           
        #logging.debug(substance_row)
        logging.debug(f"------- PS --> {substance_row['substance_name']}")
        vendor = substance_row['vendor']
        logging.debug(f"vendor : {vendor}")

        if vendor in ( "merck", "sigma", "aldrich", "sigmaaldrich", "supelco") :

            logging.debug("!!! Merck group ~~~~~~~~~~ !!")

            merck_substance = msa.SubstanceMerck()
            # add good error handling
            try:
                if substance_row['article_no'].strip() != "" :
                    merck_substance.get_by_prodID(vendor=vendor, productID=substance_row['article_no'] )
            except Exception as err:
                logging.error(f"ERROR ({err}) - no prod. no !" )
           
            if merck_substance.substance :
                substance = Substance.objects.create(**merck_substance.substance)
                self._createSubstanceInstance(substance_row, substance, 
                                             substance_properties=merck_substance.properties, 
                                             substance_metainfo=merck_substance.metainfo )
        #elif vendor == "acros":
        #    logging.debug("!!! Acros group ~~~~~~~~~~ !!")
            #acros_substance = 

        #elif vendor in ("honeywell", "fluka", "riedel-de-haen"):
        #    logging.debug("!!! honeywell group ~~~~~~~~~~ !!")
            #honeywell_substance =

        else :
            # try to get substance infos still from Merck 
            merck_substance = msa.SubstanceMerck()
            subst_links = merck_substance.get_by_CAS(CAS=substance_row['CAS'])

            if len(subst_links) == 0:
                merck_substance.substance['name'] = substance_row['substance_name']

            logging.debug(f"substance by CAS: {subst_links}")

            for subst_link in subst_links:

                #logging.debug("creating {}".format(substance) )
                ## !! check first, if exists...

                merck_substance.get_by_prod_link(product_url=subst_link)
                try:
                    logging.debug(f"substance  {merck_substance.substance['name']} found at Merck [{merck_substance.metainfo['safety_symbols']}] " )

                    if merck_substance.metainfo['safety_symbols'] is not None:
                        substance = Substance.objects.create(**merck_substance.substance)
                        self._createSubstanceInstance(substance_row, substance, 
                                             substance_properties=merck_substance.properties, 
                                             substance_metainfo=merck_substance.metainfo )
                        break

                except Exception as err:
                    logging.error(err)

        #spcalc = spc.SubstancePropCalculator()
        #spcalc.calcSubstanceProperties( smiles = substance_row['smiles'] )

        #print(spcalc.MDL)

        #Substance.objects.create( name=substance_row['name'],
                                    #~ substance_class=1, substance_functions=1,
        #                          SMILES=substance_row['smiles'],
        #                          mol_2D = spcalc.MDL)

        #~ ci_subst = ChemInfo()

        #~ acronym = substance_row['acronym']

        #~ ci_subst.pubChemCompoundByName(substance_name=substance_row['CAS'], acronym='')

        self.stdout.write(self.style.SUCCESS(f"substance {substance_row['substance_name']} added !" ))

        #~ for subst in ci_subst.substancesList() :
            #~ logging.debug("subst{}".format(subst) )
            #~ s = Substance(**subst)
            #~ s.save() # try bulk save

    def _createSubstanceInstance(self, substance_row: list, substance: Substance,
                                      substance_properties: list = [], substance_metainfo: list = []):
        """Creating an Instance of a Substance
        
        :param substance_row: [description]
        :type substance_row: list
        :param substance: [description]
        :type substance: Substance
        :param substance_metainfo: [description]
        :type substance_metainfo: list
        """        
        subst_inst = {}

        # adding amount
        try:
            subst_inst['amount_mass'] = substance_row['amount_mass']
            subst_inst['amount_vol'] = substance_row['amount_vol']
        except Exception as err:
            logging.exception

        try:
            subst_inst['registration_no'] = substance_row['no']
        except Exception as err:
            logging.exception
        
        try:
            subst_inst['substance'] = substance
            subst_inst['hazard_statements'] =  substance_metainfo['safety_hazard_statements']
            subst_inst['precautionary_statements'] = substance_metainfo['safety_precautionary_statements']
            subst_inst['hazard_symbols'] = substance_metainfo['safety_symbols']
        except Exception as err:
            sys.stderr.write(f"ERROR({err})\n")

        logging.debug(f"-----------Instance ---> {subst_inst}")

        substance_inst = SubstanceInstance.objects.create(**subst_inst)

        # adding location
        try:
            substance_inst.location = Location.objects.get(name=substance_row['location'].replace(" ","").strip())
            substance_inst.save()
        except ObjectDoesNotExist as err:
            sys.stderr.write(f"ERROR({err})\n")

        # adding vendor
        logging.debug(substance_metainfo['vendor_abbr'])
        try:
            substance_inst.vendor = Entity.objects.get(acronym=substance_metainfo['vendor_abbr'].upper())
            substance_inst.save()
        except ObjectDoesNotExist as err:
            sys.stderr.write(f"ERROR({err})\n")

