"""
________________________________________________________________________

:PROJECT: LARA

*Add substances to lara substance database*

:details: Add substances to lara substance database
         -

:file:    add_substances.py
:authors: mark doerr (mark@ismeralda.org)

:date: (creation)          20200417

.. note:: -
.. todo:: -
________________________________________________________________________
"""
__version__ = "0.2.1"


import os
import sys
import shutil
import re
import logging
import subprocess
import json
import csv

#import laralib.chem.substance_prop_calc as spc

import laralib.chem.merck_sigma_aldrich as msa
from laralib.chem.pubchem import SubstanceReaderPubChem

from django.db import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist

from django.conf import settings
#~ from lara_projects.models import Project, Experiment

from lara_django_base.models import Namespace, Location
from lara_django_people.models import Entity
from lara_django_substances.models import Substance, Polymer, Mixture
from lara_django_substances_store.models import SubstanceInstance, PolymerInstance, MixtureInstance
#~ from .chem_info import ChemInfo
from lara_django_sequences.models import SequenceClass, Sequence

class AddProteins2DB(BaseCommand):
    """Adding Protein sequence to django database, could be later expanded to other file formats, in this case, the file opening should be moved to the processing unit """
    def __init__(self, event=None, csv_filename="", replace_data=False):
        super().__init__()

        self.namespace="de.unigreifswald.biochem.akb"

        logging.debug("adding proteins now ... {}".format(csv_filename) )

        if replace_data:
            logging.warning("Replacing data in db !!!")
        try:
            with open(csv_filename, 'r' ) as csvfile:
                csv_reader = csv.DictReader(csvfile)
                self.stdout.write(self.style.SUCCESS("now adding substances ..."))
                self.addProteinSeqCSV(csv_reader)
                self.stdout.write(self.style.SUCCESS("substances added ! Have a nice day ;)"))
        except ValueError as err:
            self.stderr.write('file %s, ValueError: %s' % (csv_filename, err))
            exit()
        except KeyError as err:
            self.stderr.write('file %s, KeyError: %s\n Please check, if file is exported with quote delimited strings' % (csv_filename, err))
            exit()
        except Exception as err:
            self.stderr.write('file %s, Error: %s' % (csv_filename, err))
            exit()

    def addProteinSeqCSV(self, csv_reader):
        for seq_row in csv_reader:
            print(seq_row['name'])
            seq_row['namespace'] = Namespace.objects.get(namespace=self.namespace)
            seq_row['name_full'] = '.'.join( (self.namespace, seq_row['name']))
            seq_row['sequence_class'] = SequenceClass.objects.get(sequence_class='protein')
            Sequence.objects.create( **seq_row)

    def addProteinByNameCSV(self):
        """adding proteins by sepecifying the name"""
        for substance_row in self.csv_reader:
            pattern = re.compile(r'\s*,\s*')
            prot_name = u''.join((substance_row['PDB_code'] , '_', re.sub(pattern, '-', substance_row['variant'] ) ))

            #~ ci_subst = ChemInfo()

            #~ subst = ci_subst.accumulateProtInfo(prot_name=prot_name, PDB_code=substance_row['PDB_code'])

            #~ logging.debug(subst)

            #~ polymer = Polymer(**subst)
            #~ polymer.save() # try bulk save



