"""
________________________________________________________________________

:PROJECT: LARA

*Add substances to lara substance database*

:details: Add substances to lara substance database
         -

:file:    add_substances.py
:authors: mark doerr (mark@ismeralda.org)

:date: (creation)          20200417

.. note:: -
.. todo:: -
________________________________________________________________________
"""
__version__ = "0.2.1"


import os
import sys
import shutil
import re
import logging
import subprocess
import json
import csv

#import laralib.chem.substance_prop_calc as spc

import laralib.chem.merck_sigma_aldrich as msa
from laralib.chem.pubchem import SubstanceReaderPubChem

from django.db import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist

from django.conf import settings
#~ from lara_projects.models import Project, Experiment

from lara_django_base.models import Namespace, Location
from lara_django_people.models import Entity
from lara_django_substances.models import Substance, Polymer, Mixture
from lara_django_substances_store.models import SubstanceInstance, PolymerInstance, MixtureInstance
#~ from .chem_info import ChemInfo
from lara_django_sequences.models import SequenceClass, Sequence

class AddSequences2DB(BaseCommand):
    def __init__(self, event=None, csv_filename="", replace_data=False):
        pass #super().__init__()

        #~ logging.debug("prot_sequencing %s" % evaluation.experiments_data)
        #~ for exp_name in evaluation.experiments_data.split(";") :
            #~ curr_experiment = Proj_item.objects.get(name=exp_name)
            #~ ref_experiment = curr_experiment.reference_experiment
            #~
            #~ ref_seq = str(ref_experiment.data.first().exp_data) # reference sequence, commonly wild-type
            #~ curr_seq = str(curr_experiment.data.first().exp_data) # sequence to be compared with reference
            #~
            #~ mutations = u""
            #~ for i, curr_amino_acid in enumerate(curr_seq):
            #~ if ref_seq[i] != curr_amino_acid:
                #~ mutations +=  ref_seq[i] + str(i+1) + curr_amino_acid + ";"
                #~
            #~ # writing results to database
            #~ mutation_data = Data(  filename = "seqProtein_diff.txt",
                            #~ device = Device.objects.get(name="CGE"),
                            #~ method = Item_class.objects.get(item_class="SeqProteinDiff"),
                            #~ start_date = datetime.now().strftime("%Y%m%d"),
                            #~ start_time = datetime.now().strftime("%H%M%S"),
                            #~ exp_data = mutations )
            #~ mutation_data.save()
            #~ curr_experiment.data.add( mutation_data)

            # referencing these data:
        # str(curr_experiment.data.all().filter(method__name="SeqProteinDiff").first().exp_data)