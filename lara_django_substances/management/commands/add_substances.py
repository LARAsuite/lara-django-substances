"""
________________________________________________________________________

:PROJECT: LARA

*resetting and initialisation of substance db*

:details: add substances to LARA substance database,
          to add, e.g. a list of substances from databases, use:
          
         python3 manage.py add_substances -d substances_sm.csv
         
         python3 manage.py add_substances -s substances_demo_small.csv

:file:    models.py
:authors: mark doerr (mark@ismeralda.org)

:date: (creation)          20180324

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.1.8"

import os
import logging

from django.core.management.base import BaseCommand, CommandError
from django.core.management import  call_command

from django.conf import settings

#from lara_substances.management.add_substances_db import AddSubstances2DB, AddProteins2DB
from ._add_substances_db import AddSubstances2DB
from ._add_proteins_db import  AddProteins2DB

class Command(BaseCommand):
    """see https://docs.djangoproject.com/en/1.9/howto/custom-management-commands/ for more details
    using now new argparse mechanism of django > 1.8
    """    
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG) #level=logging.ERROR
    
    help = 'Addding / updating / removing substances in LARA substances database.'
    
    def add_arguments(self, parser):
        """ command line arguments s. https://docs.python.org/2/library/argparse.html#module-argparse """
        
        # defining named commandline arguments
        parser.add_argument('-s','--substance-csv',
            action='store',
            dest='substance_csv',
            help='substance csv file for additon or replacement of database entries')
            
        parser.add_argument('--prot-seq',
            action='store',
            help='protein sequence csv file for additon or replacement of database entries')
            
        parser.add_argument('-d','--database-collection',
            action='store',
            dest='substance_csv_db',
            help='collect substance data from databases. Substance names are provided by a csv file')
        
        # these are further switches to finer control the database collection
        parser.add_argument('-c','--chemspider-db',
            action='store_true',
            default=False,
            help='get data from chemspider database')
            
        parser.add_argument('-p','--pubchem-db',
            action='store_true',
            default=False,
            help='get data from pubchem database')
                
        parser.add_argument('-a','--all-db',
            action='store_true',
            default=False,
            help='get data from full database pipeline (pubchem -> chemspider -> ....)')
            
        parser.add_argument('-r','--replace',
            action='store_true',
            default=False,
            help='replace database entries by new data')
    
    def handle(self, *args, **options):
        """Dispatcher based on commandline arguments/options"""
            
        as2db = AddSubstances2DB()
        
        if options['substance_csv'] :
            as2db.addSubstancesCSV( csv_filename=options['substance_csv'] )
            
        elif options['substance_csv_db'] :
            as2db.addSubstancesDB( csv_filename_db=options['substance_csv_db'] )
            
        elif options['prot_seq'] :
            addseq2db = AddProteins2DB(csv_filename=options['prot_seq'] )
            
            
            
