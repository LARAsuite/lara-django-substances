# adding custum filters for display
# s. https://stackoverflow.com/questions/5827590/css-styling-in-django-forms

from django import template

register = template.Library()

@register.filter(name='addclass')
def addclass(value, arg):
    return value.as_widget(attrs={'class': arg})