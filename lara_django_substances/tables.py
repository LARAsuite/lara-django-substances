"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_substances admin *

:details: lara_django_substances admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev tables_generator lara_django_substances >> tables.py" to update this file
________________________________________________________________________
"""
# django Tests s. https://docs.djangoproject.com/en/4.1/topics/testing/overview/ for lara_django_substances
# generated with django-extensions tests_generator  lara_django_substances > tests.py (LARA-version)

import logging
import django_tables2 as tables


from .models import ExtraData, SubstanceClass, PhysicalState, Isotope, Substance, Polymer, MixtureComponent, Mixture, MoleculeResidues, Reactand, Reaction, MultiStepReaction, SubstanceToOrder

class ExtraDataTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances:extradata-detail', [tables.A('pk')]))

    class Meta:
        model = ExtraData

        fields = (
                'data_type',
                'namespace',
                'URI',
                'text',
                'XML',
                'JSON',
                'bin',
                'media_type',
                'IRI',
                'URL',
                'description',
                'file',
                'image')

class SubstanceClassTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances:substanceclass-detail', [tables.A('pk')]))

    class Meta:
        model = SubstanceClass

        fields = (
                'substance_class',
                'description')

class PhysicalStateTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances:physicalstate-detail', [tables.A('pk')]))

    class Meta:
        model = PhysicalState

        fields = (
                'state',
                'description')

class IsotopeTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances:isotope-detail', [tables.A('pk')]))

    class Meta:
        model = Isotope

        fields = (
                'name',
                'acronym',
                'synonyms',

                'CAS',
                'IUPAC_name',
                'PubChemID',
                'PubChemFingerprint',
                'ChemSpiderID',
                'BeilsteinRegNum',
                'EC_num',
                'MDL_num',
                'PDB_code',
               
                'properties',
                'sumformula',

                'SMILES',
                'IsoSMILES',
                'CanSMILES',
                'InChI',
                'InChIKey',
                'StdInChI',
                'StdInChIKey',
          
               
                'mol_raw',
                'mol_2D',
                'mol_3D',
                'svg',
                'image',
                'literature',
                'remarks',
                'description',
                'num_protons',
                'electron_config')

class SubstanceTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_substances:substance-detail', [tables.A('pk')]))

    class Meta:
        model = Substance

        fields = (
                'name',
                'acronym',
                'synonyms',

                'CAS',
                'IUPAC_name',
                'PubChemID',
                'PubChemFingerprint',
                'ChemSpiderID',
                'BeilsteinRegNum',
                'EC_num',
                'MDL_num',
                'PDB_code',
               
                'properties',
                'sumformula',

                'SMILES',
                'IsoSMILES',
                'CanSMILES',
                'InChI',
                'InChIKey',
                'StdInChI',
                'StdInChIKey',
          
               
                'mol_raw',
                'mol_2D',
                'mol_3D',
                'svg',
                'image',
                'literature',
                'remarks',
                'description',
                
                'mwt',
                'density',
                'AlogP',
                'XlogP',
                'melting_point',
                'boiling_point')

class PolymerTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_substances:polymer-detail', [tables.A('pk')]))

    class Meta:
        model = Polymer

        fields = (
                'name',
                'acronym',
                'synonyms',

                'CAS',
                'IUPAC_name',
                'PubChemID',
                'PubChemFingerprint',
                'ChemSpiderID',
                'BeilsteinRegNum',
                'EC_num',
                'MDL_num',
                'PDB_code',
               
                'properties',
                'sumformula',

                'SMILES',
                'IsoSMILES',
                'CanSMILES',
                'InChI',
                'InChIKey',
                'StdInChI',
                'StdInChIKey',
          
               
                'mol_raw',
                'mol_2D',
                'mol_3D',
                'svg',
                'image',
                'literature',
                'remarks',
                'description',
                'sequence',
                'melting_point',
                'boiling_point',
                'media_type',
                'sequence_file')

class MixtureComponentTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances:mixturecomponent-detail', [tables.A('pk')]))

    class Meta:
        model = MixtureComponent

        fields = (
                'name',
                'IRI',
                'substance',
                'polymer',
                'concentration',
                'concentration_weight',
                'description')

class MixtureTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_substances:mixture-detail', [tables.A('pk')]))

    class Meta:
        model = Mixture

        fields = (
                'name',
                'acronym',
                'synonyms',

                'CAS',
                'IUPAC_name',
                'PubChemID',
                'PubChemFingerprint',
                'ChemSpiderID',
                'BeilsteinRegNum',
                'EC_num',
                'MDL_num',
                'PDB_code',
               
                'properties',
                'sumformula',

                'SMILES',
                'IsoSMILES',
                'CanSMILES',
                'InChI',
                'InChIKey',
                'StdInChI',
                'StdInChIKey',
          
               
                'mol_raw',
                'mol_2D',
                'mol_3D',
                'svg',
                'image',
                'literature',
                'remarks',
                'description')

class MoleculeResiduesTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances:moleculeresidues-detail', [tables.A('pk')]))

    class Meta:
        model = MoleculeResidues

        fields = (
                'name',
                'substructure',
                'IRI')

class ReactandTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances:reactand-detail', [tables.A('pk')]))

    class Meta:
        model = Reactand

        fields = (
                'IRI',
                'substance',
                'polymer',
                'mixture',
                'order',
                'stoichiometric_factor',
                'physical_state',
                'literature')

class ReactionTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances:reaction-detail', [tables.A('pk')]))

    class Meta:
        model = Reaction

        fields = (
                'name',
                'label',
                'RX_UUID',
                'IRI',
                'RInChI',
                'RInChI_key_short',
                'RInChI_key_long',
                'RInChI_web_key',
                'RXNO',
                'equilibrium_const',
                'deltaG',
                'order',
                'safety_information',
                'description',
                'remarks',
                'svg',
                'image',
                'literature')

class MultiStepReactionTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances:multistepreaction-detail', [tables.A('pk')]))

    class Meta:
        model = MultiStepReaction

        fields = (
                'name',

                'description',
                'remarks',
                'svg',
                'image',
                'literature')

class SubstanceToOrderTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances:substancetoorder-detail', [tables.A('pk')]))

    class Meta:
        model = SubstanceToOrder

        fields = (
                'substance',
                'polymer',
                'mix_component',
                'mixture',
                'user',
                'datetime')

