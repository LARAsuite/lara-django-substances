"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_substances views *

:details: lara_django_substances views module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: - for search: look at old versions of the views.py file
.. todo:: -
________________________________________________________________________
"""

from dataclasses import dataclass, field
from typing import List

from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView, FormView

from django_tables2 import SingleTableView

from .models import Substance
from .models import Substance, Polymer, MixtureComponent, Mixture

from .forms import SearchForm, SubstanceCreateForm, SubstanceUpdateForm, SubstanceClassCreateForm, SubstanceClassUpdateForm, PolymerCreateForm, PolymerUpdateForm, MixtureCreateForm, MixtureUpdateForm
from .tables import SubstanceTable, PolymerTable, MixtureComponentTable, MixtureTable

# Create your  lara_django_substances views here.


@dataclass
class SubstancesMenu:
    menu_items:  List[dict] = field(default_factory=lambda: [   
        {'name': 'Substances',
         'path': 'lara_django_substances:substance-list'},
        {'name': 'Substance-Store',
                 'path': 'lara_django_substances_store:substance-list'},
        {'name': 'Polymers',
         'path': 'lara_django_substances:polymer-list'},
         {'name': 'Polymer-Store',
         'path': 'lara_django_substances_store:polymer-list'},
        {'name': 'Mixtures',
          'path': 'lara_django_substances:mixture-list'},
        {'name': 'Mixture-Store',
          'path': 'lara_django_substances_store:mixture-list'}  
    ])


class IndexView(ListView):
    model = Substance
    template_name = 'lara_django_substances/index.html'

    context_object_name = 'substance_list'


class SubstanceSafetyView(ListView):
    model = Substance
    template_name = 'lara_django_substances/safety.html'


class SubstanceSingleTableView(SingleTableView):
    model = Substance
    table_class = SubstanceTable

    #fields = ('name', 'name_full', 'URL', 'handle', 'IRI', 'manufacturer', 'model_no', 'product_type', 'type_barcode', 'product_no', 'weight', 'specifications', 'spec_JSON', 'spec_doc', 'brochure', 'quickstart', 'manual', 'service_manual', 'icon', 'image', 'description', 'substance_id', 'substance_class', 'shape')

    template_name = 'lara_django_substances/list.html'
    success_url = '/substances/substance/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Substances - List"
        context['create_link'] = 'lara_django_substances:substance-create'
        context['menu_items'] = SubstancesMenu().menu_items
        return context


class SubstanceDetailView(DetailView):
    model = Substance

    template_name = 'lara_django_substances/substance_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Substance - Details"
        context['update_link'] = 'lara_django_substances:substance-update'
        context['menu_items'] = SubstancesMenu().menu_items
        return context


class SubstanceCreateView(CreateView):
    model = Substance

    template_name = 'lara_django_substances/create_form.html'
    form_class = SubstanceCreateForm
    success_url = '/substances/substance/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Substance - Create"
        return context


class SubstanceUpdateView(UpdateView):
    model = Substance

    template_name = 'lara_django_substances/update_form.html'
    form_class = SubstanceUpdateForm
    success_url = '/substances/substance/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Substance - Update"
        context['delete_link'] = 'lara_django_substances:substance-delete'
        return context


class SubstanceDeleteView(DeleteView):
    model = Substance

    template_name = 'lara_django_substances/delete_form.html'
    success_url = '/substances/substance/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Substance - Delete"
        context['delete_link'] = 'lara_django_substances:substance-delete'
        return context


class PolymerSingleTableView(SingleTableView):
    model = Polymer
    table_class = PolymerTable

    #fields = ('name', 'name_full', 'URL', 'handle', 'IRI', 'manufacturer', 'model_no', 'product_type', 'type_barcode', 'product_no', 'weight', 'specifications', 'spec_JSON', 'spec_doc', 'brochure', 'quickstart', 'manual', 'service_manual', 'icon', 'image', 'description', 'polymer_id', 'polymer_class', 'shape')

    template_name = 'lara_django_substances/list.html'
    success_url = '/substances/polymer/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Polymer - List"
        context['create_link'] = 'lara_django_substances:polymer-create'
        context['menu_items'] = SubstancesMenu().menu_items
        return context


class PolymerDetailView(DetailView):
    model = Polymer

    template_name = 'lara_django_substances/polymer_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Polymer - Details"
        context['update_link'] = 'lara_django_substances:polymer-update'
        context['menu_items'] = SubstancesMenu().menu_items
        return context


class PolymerCreateView(CreateView):
    model = Polymer

    template_name = 'lara_django_substances/create_form.html'
    form_class = PolymerCreateForm
    success_url = '/substances/polymer/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Polymer - Create"
        return context


class PolymerUpdateView(UpdateView):
    model = Polymer

    template_name = 'lara_django_substances/update_form.html'
    form_class = PolymerUpdateForm
    success_url = '/substances/polymer/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Polymer - Update"
        context['delete_link'] = 'lara_django_substances:polymer-delete'
        return context


class PolymerDeleteView(DeleteView):
    model = Polymer

    template_name = 'lara_django_substances/delete_form.html'
    success_url = '/substances/polymer/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Polymer - Delete"
        context['delete_link'] = 'lara_django_substances:polymer-delete'
        return context

class MixtureSingleTableView(SingleTableView):
    model = Mixture
    table_class = MixtureTable

    #fields = ('name', 'name_full', 'URL', 'handle', 'IRI', 'manufacturer', 'model_no', 'product_type', 'type_barcode', 'product_no', 'weight', 'specifications', 'spec_JSON', 'spec_doc', 'brochure', 'quickstart', 'manual', 'service_manual', 'icon', 'image', 'description', 'mixture_id', 'mixture_class', 'shape')

    template_name = 'lara_django_substances/list.html'
    success_url = '/substances/mixture/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Mixture - List"
        context['create_link'] = 'lara_django_substances:mixture-create'
        context['menu_items'] = SubstancesMenu().menu_items
        return context


class MixtureDetailView(DetailView):
    model = Mixture

    template_name = 'lara_django_substances/mixture_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Mixture - Details"
        context['update_link'] = 'lara_django_substances:mixture-update'
        context['menu_items'] = SubstancesMenu().menu_items
        return context


class MixtureCreateView(CreateView):
    model = Mixture

    template_name = 'lara_django_substances/create_form.html'
    form_class = MixtureCreateForm
    success_url = '/substances/mixture/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Mixture - Create"
        return context


class MixtureUpdateView(UpdateView):
    model = Mixture

    template_name = 'lara_django_substances/update_form.html'
    form_class = MixtureUpdateForm
    success_url = '/substances/mixture/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Mixture - Update"
        context['delete_link'] = 'lara_django_substances:mixture-delete'
        return context


class MixtureDeleteView(DeleteView):
    model = Mixture

    template_name = 'lara_django_substances/delete_form.html'
    success_url = '/substances/mixture/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Mixture - Delete"
        context['delete_link'] = 'lara_django_substances:mixture-delete'
        return context


### ------------ old ---


class SubstancesListView_old(ListView):
    model = Substance
    template_name = 'lara_django_substances/substance_list.html'

    table_caption = "List of all LARA substances"
    context = {'table_caption': table_caption}


class SubstanceDetail_old(DetailView):
    model = Substance


class SubstanceCreateView_old(CreateView):
    model = Substance
    fields = '__all__'
    template_name_suffix = '_create_form'
    success_url = 'substances'


class SubstanceSearchView(FormView):
    model = Substance
    #template_name_suffix = '_search_form'
    template_name = 'substance_search_form.html'
    form_class = SearchForm
    error = []

    # form = SearchForm(request.GET)
    # form.is_valid()
    # # Basic form validation as defined in forms.py
    # if form.is_valid() == False:
    #     for item in form.errors:
    #         error.append(form[item].errors)
    #     return render(request, 'search.html', {'error': error, 'form':form})
    # moltext = form.cleaned_data['moltext']
    # smilesstring = unicode(form.cleaned_data['smiles'])
    # name = form.cleaned_data['name']
    # cas = form.cleaned_data['cas']

    # if 'properties' in request.GET:
    #     #properties and IDs search
    #     mollist = id_search(cas, name, storageid, supplierid, supplier, chn, molclass, platebarcode, samplebarcode, storage)
    #     if cas == '':
    #         mollist = properties_search(mollist, minmw, maxmw, minlogp, maxlogp, minhba, maxhba, minhbd, maxhbd, mintpsa, maxtpsa, minfsp3, maxfsp3)

    #     paginator = Paginator(mollist, 15)
    #     page = request.GET.get('page') #get value of query
    #     nofmols = len(mollist)
    #     currentpath = request.get_full_path()
    #     currentpath = get_page_wo_page(currentpath)

    #     try:
    #         mollist = paginator.page(page)
    #     except PageNotAnInteger:
    #         # If page is not an integer, deliver first page.
    #         mollist = paginator.page(1)
    #     except EmptyPage:
    #         # If page is out of range (e.g. 9999), deliver last page of results.
    #         mollist = paginator.page(paginator.num_pages)
    #     return render(request, 'browse.html', {'mollist':mollist,'currpage':page, 'ofmols':nofmols, 'currentpath':currentpath})
    # else:
    #     form = SearchForm()
    # return render(request, 'search.html', {'error': error, 'form':form})

    # success_url = 'search-results'


class SubstanceSearchResults(ListView):
    model = Substance
    template_name_suffix = '_search_results'


def browse(request):
    mollist = Substance.objects.all()

    nofmols = len(mollist)
    return render(request, 'lara_django_substances/substance_browse.html', {'mollist': mollist, 'ofmols': nofmols, 'currentpath': '?'})


def molecule(request, idnum):
    # Views for individual molecules
    try:
        offset = int(idnum)
    except ValueError:
        raise Http404()
    mol = get_object_or_404(Molecule, pk=idnum)
    quantity = str(mol.amount) + " " + str(mol.unit)
    return render(request, 'molecule.html', {'mol': mol, 'molname': mol.name, 'smiles': mol.SMILES, 'cas': mol.CAS, 'altname': mol.altname, 'molfile': mol.molfile, 'mw': mol.CMW, 'chn': mol.CHN, 'hba': mol.HBA, 'hbd': mol.HBD, 'logp': mol.logP, 'tpsa': mol.tpsa, 'supp': mol.supplier, 'suppid': mol.supplierID, 'storage': mol.storageID, 'comm': mol.comment, 'quantity': quantity, 'added': mol.added})


def search(request):
    error = []
    form = SearchForm(request.GET)
    form.is_valid()
    # Basic form validation as defined in forms.py
    if form.is_valid() == False:
        for item in form.errors:
            error.append(form[item].errors)
        return render(request, 'search.html', {'error': error, 'form': form})
    moltext = form.cleaned_data['moltext']
    smilesstring = unicode(form.cleaned_data['smiles'])
    minmw = form.cleaned_data['minmw']
    maxmw = form.cleaned_data['maxmw']
    minlogp = form.cleaned_data['minlogp']
    maxlogp = form.cleaned_data['maxlogp']
    minhba = form.cleaned_data['minhba']
    maxhba = form.cleaned_data['maxhba']
    minhbd = form.cleaned_data['minhbd']
    maxhbd = form.cleaned_data['maxhbd']
    mintpsa = form.cleaned_data['mintpsa']
    maxtpsa = form.cleaned_data['maxtpsa']
    minfsp3 = form.cleaned_data['minfsp3']
    maxfsp3 = form.cleaned_data['maxfsp3']
    chn = form.cleaned_data['chn']
    name = form.cleaned_data['name']
    storageid = form.cleaned_data['storageid']
    supplierid = form.cleaned_data['supplierid']
    supplier = form.cleaned_data['supplier']
    cas = form.cleaned_data['cas']
    molclass = form.cleaned_data['molclass']
    platebarcode = form.cleaned_data['platebarcode']
    samplebarcode = form.cleaned_data['samplebarcode']
    storage = form.cleaned_data['storage']

    # Handling of mol or smi input
    if moltext:
        moltext = pybel.readstring("mol", str(moltext))
        smiles = moltext.write("smi").split("\t")[0]
        print(smiles)
    elif smilesstring:
        smiles = str(smilesstring)
        print(smiles)

    if 'similarity' in request.GET or 'substructure' in request.GET:
        try:
            test = pybel.readstring("smi", smiles)
            tanimoto = float(form.cleaned_data['tanimoto'])
        except:
            error.append("Molecule or tanimoto index not valid!")

    if error:
        return render(request, 'search.html', {'error': error, 'form': form})

    if 'similarity' in request.GET and smiles:
        # similartiy search
        mollist = id_search(cas, name, storageid, supplierid, supplier,
                            chn, molclass, platebarcode, samplebarcode, storage)
        mollist = fast_fp_search(mollist, smiles, tanimoto)
        mollist = properties_search(mollist, minmw, maxmw, minlogp, maxlogp,
                                    minhba, maxhba, minhbd, maxhbd, mintpsa, maxtpsa, minfsp3, maxfsp3)
        paginator = Paginator(mollist, 15)
        page = request.GET.get('page')  # get value of query
        nofmols = len(mollist)
        currentpath = request.get_full_path()
        currentpath = get_page_wo_page(currentpath)
        try:
            mollist = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            mollist = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            mollist = paginator.page(paginator.num_pages)
        return render(request, 'browse.html', {'mollist': mollist, 'currpage': page, 'ofmols': nofmols, 'currentpath': currentpath})
    elif 'substructure' in request.GET and smiles:
        # substructure search
        smarts = smiles
        mollist = id_search(cas, name, storageid, supplierid, supplier,
                            chn, molclass, platebarcode, samplebarcode, storage)
        mollist = smarts_search(mollist, smarts)
        mollist = properties_search(mollist, minmw, maxmw, minlogp, maxlogp,
                                    minhba, maxhba, minhbd, maxhbd, mintpsa, maxtpsa, minfsp3, maxfsp3)
        paginator = Paginator(mollist, 15)
        page = request.GET.get('page')  # get value of query
        nofmols = len(mollist)
        currentpath = request.get_full_path()
        currentpath = get_page_wo_page(currentpath)
        try:
            mollist = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            mollist = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            mollist = paginator.page(paginator.num_pages)
        return render(request, 'browse.html', {'mollist': mollist, 'currpage': page, 'ofmols': nofmols, 'currentpath': currentpath})
    elif 'properties' in request.GET:
        # properties and IDs search
        mollist = id_search(cas, name, storageid, supplierid, supplier,
                            chn, molclass, platebarcode, samplebarcode, storage)
        if cas == '':
            mollist = properties_search(mollist, minmw, maxmw, minlogp, maxlogp,
                                        minhba, maxhba, minhbd, maxhbd, mintpsa, maxtpsa, minfsp3, maxfsp3)

        paginator = Paginator(mollist, 15)
        page = request.GET.get('page')  # get value of query
        nofmols = len(mollist)
        currentpath = request.get_full_path()
        currentpath = get_page_wo_page(currentpath)

        try:
            mollist = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            mollist = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            mollist = paginator.page(paginator.num_pages)
        return render(request, 'browse.html', {'mollist': mollist, 'currpage': page, 'ofmols': nofmols, 'currentpath': currentpath})
    else:
        form = SearchForm()
    return render(request, 'search.html', {'error': error, 'form': form})


def statistics(request):
    logp = []
    mw = []
    hba = []
    hbd = []
    tpsa = []
    names = []
    fsp3 = []
    nrb = []
    complexity = []
    for mol in Molecule.objects.all():
        try:
            float(mol.logP)
            float(mol.CMW)
            float(mol.HBA)
            float(mol.HBD)
            float(mol.tpsa)
            float(mol.fsp3)
            float(mol.nrb)
            float(mol.complexity)
            if mol.CMW <= 800:
                logp.append(mol.logP)
                mw.append(mol.CMW)
                hba.append(mol.HBA)
                hbd.append(mol.HBD)
                tpsa.append(mol.tpsa)
                fsp3.append(mol.fsp3*100)
                nrb.append(mol.nrb)
                complexity.append(mol.complexity)
                if mol.name == "":
                    names.append("X")
                else:
                    names.append(mol.name)
        except:
            pass
    logpmw = dumps(zip(logp, mw))
    hbamw = dumps(zip(hba, mw))
    hbdmw = dumps(zip(hbd, mw))
    tpsamw = dumps(zip(tpsa, mw))
    # histograms
    mwhist, mwedge = histogram(mw, bins=range(0, 800, 50))
    mwhist = dumps(zip(array(mwedge[:-1]).tolist(), array(mwhist).tolist()))
    fsp3hist, fsp3edge = histogram(fsp3, bins=20)
    fsp3hist = dumps(
        zip(array(fsp3edge[:-1]).tolist(), array(fsp3hist).tolist()))
    nrbhist, nrbedge = histogram(nrb, bins=[x + 0.5 for x in range(-1, 26, 1)])
    nrbhist = dumps(zip(array(nrbedge[:-1]).tolist(), array(nrbhist).tolist()))
    complexityhist, complexityedge = histogram(
        complexity, bins=range(0, 160, 10))
    complexityhist = dumps(
        zip(array(complexityedge[:-1]).tolist(), array(complexityhist).tolist()))
    logphist, logpedge = histogram(
        logp, bins=[x + 0.5 for x in range(-10, 10, 1)])
    logphist = dumps(
        zip(array(logpedge[:-1]).tolist(), array(logphist).tolist()))
    hbahist, hbaedge = histogram(hba, bins=[x + 0.5 for x in range(-1, 21, 1)])
    hbahist = dumps(zip(array(hbaedge[:-1]).tolist(), array(hbahist).tolist()))
    hbdhist, hbdedge = histogram(hbd, bins=[x + 0.5 for x in range(-1, 11, 1)])
    hbdhist = dumps(zip(array(hbdedge[:-1]).tolist(), array(hbdhist).tolist()))
    tpsahist, tpsaedge = histogram(tpsa, bins=range(0, 250, 10))
    tpsahist = dumps(
        zip(array(tpsaedge[:-1]).tolist(), array(tpsahist).tolist()))
    return render(request, 'statistics.html', {'logpmw': logpmw, 'hbamw': hbamw, 'hbdmw': hbdmw, 'tpsamw': tpsamw, 'mwhist': mwhist, 'fsp3hist': fsp3hist, 'nrbhist': nrbhist, 'complexityhist': complexityhist, 'logphist': logphist, 'hbahist': hbahist, 'hbdhist': hbdhist, 'tpsahist': tpsahist})


def searchForm(request):
    return render(request, 'lara_django_substances/search.html', context={})


def search_old(request):
    substance_list = []
    search_string = request.POST['search']
    try:
        curr_substance = Substance.objects.get(acronym=search_string)
        if curr_substance:
            substance_list.append(curr_substance)
        else:
            curr_substance = Substance.objects.get(name=search_string)
            if curr_substance:
                substance_list.append(curr_substance)

    except (ValueError):
        logging.error("Error")
        # ~ return render(request, 'polls/detail.html', {
        # ~ 'question': question,
        # ~ 'error_message': "You didn't select a choice.",
        # ~ })
    else:
        context = {'substance_list': substance_list}
        return render(request, 'lara_django_substances/results.html', context)
        # return HttpResponseRedirect(reverse('lara_django_substances:results', args=(lara_device_list,)))


def results(request, substance_list):
    context = {'substance_list': substance_list}
    return render(request, 'lara_django_substances/results.html', context)


# ~ class IndexView(ListView) :
    # ~ model =  Substance
    # ~ template_name = 'lara_django_substances/index.html'

# ~ class DevicesListView(ListView) :
    # ~ model = Device
    # ~ template_name = 'lara_devices/device_list.html'

    # ~ table_caption = "List of all LARA devices"
    # ~ context = {'table_caption': table_caption}

# ~ def searchForm(request):
    # ~ return render(request, 'lara_devices/search.html', context={} )

# ~ class DeviceDetails(DetailView) :
    # ~ model = Device
